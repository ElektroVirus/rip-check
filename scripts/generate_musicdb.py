#!/usr/bin/python

import subprocess
import os
import shutil

MUSIC_DB_DIR="flac_mockup"

def main():
    """
    MusicDB to be created

    First dict has album name as key and second dict as value.
    Second dict has song filename as key and list of metadata-tags value,
    which are to be set onto that file.
    """
    MUSIC_DB = {
        "Ensiferum - Dragonheads - 2006": {
            "01 - Ensiferum - Dragonheads.flac": [
                "CTDBDISCCONFIDENCE=132/132",
                "CTDBTRACKCONFIDENCE=132/132",
                "CDTOC=6+96+5EB6+B491+D41E+12B01+16E2A+1C901",
                "TRACKTOTAL=6",
                "TRACKNUMBER=01",
                "TITLE=Dragonheads",
                "ALBUM=Dragonheads",
                "ARTIST=Ensiferum",
                "DISCTOTAL=1",
                "DISCNUMBER=1",
                "DATE=2006",
                "COMMENT=CUERipper v2.1.8 Copyright (C) 2008-2021 Grigory Chudov",
                "RELEASE DATE=2006-03-31",
                "RELEASECOUNTRY=DE",
                "PUBLISHER=Spinefarm Records",
                "LABELNO=SPI256CD",
            ],
            "02 - Ensiferum - Warrior's Quest.flac": [
                "CTDBDISCCONFIDENCE=132/132",
                "CTDBTRACKCONFIDENCE=132/132",
                "CDTOC=6+96+5EB6+B491+D41E+12B01+16E2A+1C901",
                "TRACKTOTAL=6",
                "TRACKNUMBER=02",
                "TITLE=Warrior's Quest",
                "ALBUM=Dragonheads",
                "ARTIST=Ensiferum",
                "DISCTOTAL=1",
                "DISCNUMBER=1",
                "DATE=2006",
                "COMMENT=CUERipper v2.1.8 Copyright (C) 2008-2021 Grigory Chudov",
                "RELEASE DATE=2006-03-31",
                "RELEASECOUNTRY=DE",
                "PUBLISHER=Spinefarm Records",
                "LABELNO=SPI256CD",
            ],
            "03 - Ensiferum - Kalevala Melody.flac": [
                "CTDBDISCCONFIDENCE=132/132",
                "CTDBTRACKCONFIDENCE=132/132",
                "CDTOC=6+96+5EB6+B491+D41E+12B01+16E2A+1C901",
                "TRACKTOTAL=6",
                "TRACKNUMBER=03",
                "TITLE=Kalevala Melody",
                "ALBUM=Dragonheads",
                "ARTIST=Ensiferum",
                "DISCTOTAL=1",
                "DISCNUMBER=1",
                "DATE=2006",
                "COMMENT=CUERipper v2.1.8 Copyright (C) 2008-2021 Grigory Chudov",
                "RELEASE DATE=2006-03-31",
                "RELEASECOUNTRY=DE",
                "PUBLISHER=Spinefarm Records",
                "LABELNO=SPI256CD",
            ],
            "04 - Ensiferum - White Storm.flac": [
                "CTDBDISCCONFIDENCE=132/132",
                "CTDBTRACKCONFIDENCE=132/132",
                "CDTOC=6+96+5EB6+B491+D41E+12B01+16E2A+1C901",
                "TRACKTOTAL=6",
                "TRACKNUMBER=04",
                "TITLE=White Storm",
                "ALBUM=Dragonheads",
                "ARTIST=Ensiferum",
                "DISCTOTAL=1",
                "DISCNUMBER=1",
                "DATE=2006",
                "COMMENT=CUERipper v2.1.8 Copyright (C) 2008-2021 Grigory Chudov",
                "RELEASE DATE=2006-03-31",
                "RELEASECOUNTRY=DE",
                "PUBLISHER=Spinefarm Records",
                "LABELNO=SPI256CD",
            ],
            "05 - Ensiferum - Into Hiding.flac": [
                "CTDBDISCCONFIDENCE=132/132",
                "CTDBTRACKCONFIDENCE=132/132",
                "CDTOC=6+96+5EB6+B491+D41E+12B01+16E2A+1C901",
                "TRACKTOTAL=6",
                "TRACKNUMBER=05",
                "TITLE=Into Hiding",
                "ALBUM=Dragonheads",
                "ARTIST=Ensiferum",
                "DISCTOTAL=1",
                "DISCNUMBER=1",
                "DATE=2006",
                "COMMENT=CUERipper v2.1.8 Copyright (C) 2008-2021 Grigory Chudov",
                "RELEASE DATE=2006-03-31",
                "RELEASECOUNTRY=DE",
                "PUBLISHER=Spinefarm Records",
                "LABELNO=SPI256CD",
            ],
            "06 - Ensiferum - Finnish Medley_ Karjalan kunnailla _ Myrskyluodon Maija _ Metsämiehen laulu.flac": 
            [
                "CTDBDISCCONFIDENCE=132/132",
                "CTDBTRACKCONFIDENCE=132/132",
                "CDTOC=6+96+5EB6+B491+D41E+12B01+16E2A+1C901",
                "TRACKTOTAL=6",
                "TRACKNUMBER=06",
                "TITLE=Finnish Medley: Karjalan kunnailla / Myrskyluodon Maija / Metsämiehen laulu",
                "ALBUM=Dragonheads",
                "ARTIST=Ensiferum",
                "DISCTOTAL=1",
                "DISCNUMBER=1",
                "DATE=2006",
                "COMMENT=CUERipper v2.1.8 Copyright (C) 2008-2021 Grigory Chudov",
                "RELEASE DATE=2006-03-31",
                "RELEASECOUNTRY=DE",
                "PUBLISHER=Spinefarm Records",
                "LABELNO=SPI256CD",
            ],
        },
        "Testartist0 - Testalbum0 - 9999": {
            "01 - THIS_SONG_DOES_NOT_HAVE_ANY_METADATA_FIELDS.flac": [],
            # "02 - this_song_has_falsely_named_jotain.flac": [],
        },
    }

    populate_musicdb(MUSIC_DB)

def create_flac_file(fs_path: str) -> None:
    dirname = os.path.dirname(fs_path)
    os.makedirs(dirname, exist_ok=True)

    COMMAND_FLAC = f"flac -f --no-padding --force-raw-format --endian=little \
            --sign=signed --channels=2 --bps=16 --sample-rate=44100 \
            --output-name \"{fs_path}\" - </dev/null >/dev/null 2>/dev/null"

    subprocess.run(COMMAND_FLAC, shell=True)

def write_tag(fs_path: str, tag: str) -> None:
    COMMAND_METAFLAC = f"metaflac --set-tag=\"{tag}\" \"{fs_path}\""
    subprocess.run(COMMAND_METAFLAC, shell=True)

def populate_musicdb(music_db):
    shutil.rmtree(MUSIC_DB_DIR, ignore_errors=True)
    for album in music_db.keys():
        for song in music_db[album].keys():
            song_path = f"{MUSIC_DB_DIR}/{album}/{song}"
            create_flac_file(song_path)
            for tag in music_db[album][song]:
                write_tag(song_path, tag)

if __name__ == "__main__":
    main()
