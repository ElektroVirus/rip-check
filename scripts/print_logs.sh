#!/bin/bash
set -euo pipefail
fd  "ripcheck.log" flac/ --exec-batch tail -n +1 {}
