use std::path::PathBuf;

use crate::{music_db::MusicDb, music_db_check::check_db, ripcheck_settings::RipCheckSettings};
use env_logger::Env;

mod music_db;
mod music_db_check;
mod ripcheck_logger;
mod ripcheck_settings;
mod util;

fn main() {
    // TODO: Use clap command line parser
    // https://docs.rs/clap/latest/clap/

    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    test_logger();

    let settings = RipCheckSettings::new();
    settings.debug_print();

    let dir_flac = settings.config.get_table("rip-check").unwrap()["path"].to_string();
    let dir_checks =
        settings.config.get_table("rip-check").unwrap()["external_checks_path"].to_string();
    let dry_run: bool = settings.config.get_table("rip-check").unwrap()["dry-run"]
        .to_string()
        .parse()
        .unwrap();
    let quiet: bool = settings.config.get_table("rip-check").unwrap()["quiet"]
        .to_string()
        .parse()
        .unwrap();

    let db;
    if let Ok(musicdb) = MusicDb::try_from_dir(&PathBuf::from(&dir_flac)) {
        db = musicdb;
    } else {
        panic!("MusicDb could not be constructed from {}", &dir_flac);
    }

    println!("------ MusicDB ------");
    println!("{:?}", db);
    println!("---------------------");
    println!("MusicDb memory usage: {} bytes", db.size_in_mem());
    println!("---------------------");
    println!("--- db.check_db() ---");

    check_db(&db, &PathBuf::from(dir_checks), quiet, dry_run).unwrap();
}

fn test_logger() {
    use crate::ripcheck_logger::{RipCheckLogger, RipCheckLoggerMessage};

    let mut rclogger = RipCheckLogger::builder().color_tty(true).build();
    rclogger
        .log(&RipCheckLoggerMessage::TestSucceeded(
            "main.rs::test_logger",
            "Testing RipCheckLogger",
        ))
        .unwrap();
    rclogger
        .log(&RipCheckLoggerMessage::TestFailed(
            "main.rs::test_logger",
            "Testing RipCheckLogger",
        ))
        .unwrap();
    rclogger
        .log(&RipCheckLoggerMessage::Info(
            "main.rs::test_logger",
            "Testing RipCheckLogger",
        ))
        .unwrap();
    rclogger
        .log(&RipCheckLoggerMessage::Warning(
            "main.rs::test_logger",
            "Testing RipCheckLogger",
        ))
        .unwrap();
    rclogger
        .log(&RipCheckLoggerMessage::Error(
            "main.rs::test_logger",
            "Testing RipCheckLogger",
        ))
        .unwrap();
    println!();
}
