use crate::{music_db::entry::Entry, music_db::music_db_error::MusicDbError};

use fs_err as fs;
use std::fmt;
use std::path::Path;

pub mod accuraterip;
pub mod album_data;
pub mod disc_data;
mod disc_filetype;
pub mod entry;
pub mod music_db_error;
pub mod trackdata;

pub struct MusicDb {
    db: Vec<Entry>,
}

impl MusicDb {
    pub fn new() -> Self {
        MusicDb {
            db: Vec::<Entry>::new(),
        }
    }

    pub fn try_from_dir(path: &Path) -> Result<Self, MusicDbError> {
        if !path.exists() || !path.is_dir() {
            return Err(MusicDbError::MusicDbNotFound(path.to_path_buf()));
        }

        let mut music_db = Self::new();

        let paths = fs::read_dir(path)?;
        // Iterate over directories in directory
        for path in paths {
            let fpath = path?.path();

            if let Ok(entry) = Entry::entry_from_dir(&fpath) {
                // println!("Entry memory usage: {}", entry.size_in_mem());
                music_db.add_album(entry);
            } else {
                let err_msg = format!("Directory {:?} could not be parsed as album", fpath);
                eprintln!("{}", err_msg);
            }
        }
        Ok(music_db)
    }

    pub fn add_album(&mut self, entry: Entry) {
        self.db.push(entry);
    }

    pub fn db(&self) -> &Vec<Entry> {
        &self.db
    }

    pub fn size_in_mem(&self) -> usize {
        let mut total_mem: usize = 0;
        for entry in &self.db {
            total_mem += entry.size_in_mem();
        }
        total_mem
    }
}

impl fmt::Display for MusicDb {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for entry in &self.db {
            writeln!(f, "{}", entry)?;
        }
        Ok(())
    }
}

impl fmt::Debug for MusicDb {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for entry in &self.db {
            writeln!(f, "{:?}", entry)?;
        }
        Ok(())
    }
}
