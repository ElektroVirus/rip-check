use core::fmt;
use std::collections::HashMap;
pub(crate) use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};

use super::music_db_error::MusicDbError;

impl AccurateRip {
    pub fn builder(path: &Path) -> AccurateRipBuilder {
        AccurateRipBuilder::new(path)
    }

    pub fn try_from_path(path: &Path) -> Result<Self, MusicDbError> {
        let file = match File::open(path) {
            Err(_e) => return Err(MusicDbError::AccuripPathNotFound(path.to_path_buf())),
            Ok(res) => res,
        };

        let mut next_section = AccurateLogSection::Start;
        let mut arb = AccurateRip::builder(path);
        let reader = BufReader::new(file);

        // Temp variables
        let mut last_offset_header = None;
        let mut ctdbid_status_vec = Vec::<CtdbIdStatus>::new();
        let mut ctdb_track_status_vec = Vec::<CtdbTrackStatus>::new();
        let mut track_status_crc_v2_vec = Vec::<TrackStatusCrcV2>::new();
        let mut offsets = HashMap::<i32, Vec<TrackOffsetStatus>>::new();
        let mut track_peak_vec = Vec::<TrackPeak>::new();
        for (i, line) in reader.lines().enumerate() {
            if let Ok(l) = line {
                if is_comment_line(&l) {
                    arb.comment(&l);
                } else if is_ctdb_tocid_line(&l) {
                    if let Ok(ctdb_tocid) = parse_ctdb_tocid(&l) {
                        arb.ctdb_tocid(ctdb_tocid);
                    }
                } else if is_ctbid_status_header(&l) {
                    next_section = AccurateLogSection::CtbidStatus;
                    continue;
                } else if is_track_ctdb_status_header(&l) {
                    next_section = AccurateLogSection::TrackCtdbStatus;
                    continue;
                } else if is_track_crc_v2_status_header(&l) {
                    next_section = AccurateLogSection::TrackCrcV2Status;
                    continue;
                } else if is_offset_header(&l) {
                    next_section = AccurateLogSection::Offset;
                    last_offset_header = parse_offset_from_offset_header(&l).ok();
                    continue;
                } else if is_track_peak_header(&l) {
                    next_section = AccurateLogSection::TrackPeak;
                    continue;
                } else if is_accuraterip_id_line(&l) {
                    if let Ok(arid) = parse_accurate_rip_id(&l) {
                        arb.accuraterip_id(arid);
                    }
                    continue;
                }

                match next_section {
                    AccurateLogSection::Start => {}
                    AccurateLogSection::CtbidStatus => {
                        if let Ok(res) = parse_ctdbid_status_line(&l) {
                            ctdbid_status_vec.push(res);
                        }
                    }
                    AccurateLogSection::TrackCtdbStatus => {
                        if let Ok(res) = parse_track_ctdb_status_line(&l) {
                            ctdb_track_status_vec.push(res);
                        }
                    }
                    AccurateLogSection::TrackCrcV2Status => {
                        if let Ok(res) = parse_track_status_crc_v2_line(&l) {
                            track_status_crc_v2_vec.push(res);
                        }
                    }
                    AccurateLogSection::Offset => {
                        if let Ok(res) = parse_track_offset_line(&l) {
                            if let Some(key) = last_offset_header {
                                offsets.entry(key).or_insert_with(Vec::new);
                                if let Some(ref mut vec) = offsets.get_mut(&key) {
                                    vec.push(res);
                                }
                            } else {
                                return Err(MusicDbError::AccuripParseError(
                                    "No previous last_offset_header defined!".to_owned(),
                                    path.to_path_buf(),
                                ));
                            }
                        }
                    }
                    AccurateLogSection::TrackPeak => {
                        if let Ok(res) = parse_track_peak_line(&l) {
                            track_peak_vec.push(res);
                        } else {
                            return Err(MusicDbError::AccuripParseError(
                                format!("Track peak parse failed on line #{}: '{}'", i, l),
                                path.to_path_buf(),
                            ));
                        }
                    }
                }
            }
        }
        arb.ctdbid_status(ctdbid_status_vec);
        arb.ctdb_track_status(ctdb_track_status_vec);
        arb.track_crc_v2_status(track_status_crc_v2_vec);
        arb.track_offset_status(offsets);
        arb.track_peak(track_peak_vec);

        Ok(arb.build())
    }
}

impl fmt::Display for AccurateRip {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "fs_path: {:?}", self.fs_path)?;
        writeln!(f, "comment: {:?}", self.comment)?;
        writeln!(f, "ctdb_tocid: {:?}", self.ctdb_tocid)?;
        writeln!(f, "accuraterip_id: {:?}", self.accuraterip_id)?;

        writeln!(f, "-- ctdb_status -- ")?;
        for ctdbid_status in &self.ctdbid_status {
            writeln!(f, "{:?}", ctdbid_status)?;
        }

        writeln!(f, "-- track_ctdb_status -- ")?;
        for track_ctdb_status in &self.track_ctdb_status {
            writeln!(f, "{:?}", track_ctdb_status)?;
        }

        writeln!(f, "-- track_crc_v2_status -- ")?;
        for track_crc_v2_status in &self.track_crc_v2_status {
            writeln!(f, "{:?}", track_crc_v2_status)?;
        }

        writeln!(f, "-- track_offset_status -- ")?;
        for key in self.track_offset_status.keys() {
            writeln!(f, "Offset {}:", key)?;
            for track_offset_status in &self.track_offset_status[key] {
                writeln!(f, "{:?}", track_offset_status)?;
            }
        }

        writeln!(f, "-- track_peak -- ")?;
        for track_peak in &self.track_peak {
            writeln!(f, "{:?}", track_peak)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum CtdbIdStatusMessage {
    AccuratelyRipped,
    NoMatch,
    HasNodataTrackNomatch,
    Other(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum CtdbTrackStatusMessage {
    AccuratelyRipped,
    Other(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum TrackStatusCrcV2Message {
    AccuratelyRipped,
    NoMatch,
    Other(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum TrackOffsetStatusMessage {
    AccuretelyRipped,
    NoMatch,
    NoMatchV2NotTested,
    Other(String),
}

#[derive(Debug, Clone, PartialEq)]
pub struct CtdbIdStatus {
    ctdbid: String,
    status: (u16, u16),
    msg: CtdbIdStatusMessage,
}

#[derive(Debug, Clone, PartialEq)]
pub struct CtdbTrackStatus {
    track_nro: u16,
    status: (u16, u16),
    msg: CtdbTrackStatusMessage,
}

#[derive(Debug, Clone, PartialEq)]
pub struct TrackStatusCrcV2 {
    track_nro: u16,
    crc: String,
    crc_v2: String,
    status: (u16, u16, u16),
    msg: TrackStatusCrcV2Message,
}

#[derive(Debug, Clone, PartialEq)]
pub struct TrackOffsetStatus {
    track_nro: u16,
    crc: String,
    status: (u16, u16),
    msg: TrackOffsetStatusMessage,
}

#[derive(Debug, Clone, PartialEq)]
pub struct TrackPeak {
    track_nro: Option<u16>,
    peak: f32,
    crc32: String,
    w_o_null: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct CtdbTocId {
    id: String,
    found: bool,
}

#[derive(Debug, Clone, PartialEq)]
pub struct AccurateRipId {
    id: String,
    found: bool,
}

#[derive(Debug, Clone, PartialEq)]
pub struct AccurateRip {
    fs_path: PathBuf,              // Path of the .accurip -file
    comment: Option<String>,       // Comment on the first line
    ctdb_tocid: Option<CtdbTocId>, //
    ctdbid_status: Vec<CtdbIdStatus>,
    track_ctdb_status: Vec<CtdbTrackStatus>,
    accuraterip_id: Option<AccurateRipId>,
    track_crc_v2_status: Vec<TrackStatusCrcV2>,
    track_offset_status: HashMap<i32, Vec<TrackOffsetStatus>>,
    track_peak: Vec<TrackPeak>,
}

#[derive(Debug)]
enum AccurateLogSection {
    Start,
    CtbidStatus,
    TrackCtdbStatus,
    TrackCrcV2Status,
    Offset,
    TrackPeak,
}

fn is_comment_line(line: &str) -> bool {
    if line.trim().starts_with("[CUETools log;") {
        return true;
    }
    false
}

fn is_ctbid_status_header(line: &str) -> bool {
    if line.trim() == "[ CTDBID ] Status" {
        return true;
    }
    false
}

fn is_track_ctdb_status_header(line: &str) -> bool {
    if line.trim() == "Track | CTDB Status" {
        return true;
    }
    false
}

fn is_track_crc_v2_status_header(line: &str) -> bool {
    if line.trim() == "Track   [  CRC   |   V2   ] Status" {
        return true;
    }
    false
}

fn is_offset_header(line: &str) -> bool {
    if line.trim().starts_with("Offsetted by") {
        return true;
    }
    false
}

fn is_track_peak_header(line: &str) -> bool {
    if line.trim() == "Track Peak [ CRC32  ] [W/O NULL]" {
        return true;
    }
    false
}

fn is_accuraterip_id_line(line: &str) -> bool {
    if line.starts_with("[AccurateRip ID: ") {
        return true;
    }
    false
}

fn is_ctdb_tocid_line(line: &str) -> bool {
    if line.starts_with("[CTDB TOCID:") {
        return true;
    }
    false
}

fn parse_offset_from_offset_header(line: &str) -> Result<i32, ()> {
    let split = line
        .split([' ', ':'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split[0] == "Offsetted" && split[1] == "by" {
        let offset = match split[2].parse::<i32>() {
            Ok(res) => res,
            Err(_) => return Err(()),
        };
        return Ok(offset);
    }
    Err(())
}

fn parse_ctdbid_status_line(line: &str) -> Result<CtdbIdStatus, ()> {
    let split = line
        .split(['[', ']', '(', '/', ')'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() != 4 {
        return Err(());
    }

    let status_numerator = match split[1].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_denominator = match split[2].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };

    // TODO: List not comprehensive
    let msg = if split[3] == "Accurately ripped" {
        CtdbIdStatusMessage::AccuratelyRipped
    } else if split[3] == "No match" {
        CtdbIdStatusMessage::NoMatch
    } else if split[3] == "Has no data track, No match" {
        CtdbIdStatusMessage::HasNodataTrackNomatch
    } else {
        // eprintln!("CtdbIdStatusMessage::Other: {}", split[3]);
        CtdbIdStatusMessage::Other(split[3].to_owned())
    };

    Ok(CtdbIdStatus {
        ctdbid: split[0].to_owned(),
        status: (status_numerator, status_denominator),
        msg,
    })
}

fn parse_track_ctdb_status_line(line: &str) -> Result<CtdbTrackStatus, ()> {
    let split = line
        .split(['|', '(', '/', ')'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() != 4 {
        return Err(());
    }

    let track_nro = match split[0].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_numerator = match split[1].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_denominator = match split[2].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };

    // TODO: List not comprehensive
    let msg = if split[3] == "Accurately ripped" {
        CtdbTrackStatusMessage::AccuratelyRipped
    } else {
        // eprintln!("CtdbTrackStatusMessage::Other: {}", split[3]);
        CtdbTrackStatusMessage::Other(split[3].to_owned())
    };

    Ok(CtdbTrackStatus {
        track_nro,
        status: (status_numerator, status_denominator),
        msg,
    })
}

fn parse_track_status_crc_v2_line(line: &str) -> Result<TrackStatusCrcV2, ()> {
    let split = line
        .split(['[', '|', ']', '(', '+', '/', ')'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() != 7 {
        return Err(());
    }

    let track_nro = match split[0].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_prefix = match split[3].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_numerator = match split[4].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_denominator = match split[5].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };

    // TODO: List not comprehensive
    let msg = if split[6] == "Accurately ripped" {
        TrackStatusCrcV2Message::AccuratelyRipped
    } else if split[6] == "No match" {
        TrackStatusCrcV2Message::NoMatch
    } else {
        // eprintln!("TrackStatusCrcV2Message::Other: {}", split[6]);
        TrackStatusCrcV2Message::Other(split[6].to_owned())
    };

    Ok(TrackStatusCrcV2 {
        track_nro,
        crc: split[1].to_owned(),
        crc_v2: split[2].to_owned(),
        status: (status_prefix, status_numerator, status_denominator),
        msg,
    })
}

fn parse_track_offset_line(line: &str) -> Result<TrackOffsetStatus, ()> {
    let split = line
        .split(['[', ']', '(', '/', ')'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() < 5 {
        return Err(());
    }

    let track_nro = match split[0].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_numerator = match split[2].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };
    let status_denominator = match split[3].parse::<u16>() {
        Ok(res) => res,
        Err(_) => return Err(()),
    };

    let msg = match line.split_once(')') {
        Some(res) => res.1.trim().to_owned(),
        None => "".to_owned(),
    };

    // TODO: List not comprehensive
    let msg_enum = if msg == "Accurately ripped" {
        TrackOffsetStatusMessage::AccuretelyRipped
    } else if msg == "No match" {
        TrackOffsetStatusMessage::NoMatch
    } else if msg == "No match (V2 was not tested" {
        TrackOffsetStatusMessage::NoMatchV2NotTested
    } else {
        // eprintln!("TrackOffsetStatusMessage::Other: {}", msg);
        TrackOffsetStatusMessage::Other(msg)
    };

    Ok(TrackOffsetStatus {
        track_nro,
        crc: split[1].to_owned(),
        status: (status_numerator, status_denominator),
        msg: msg_enum,
    })
}

fn parse_track_peak_line(line: &str) -> Result<TrackPeak, ()> {
    let split = line
        .split([' ', '[', ']'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() != 4 {
        eprintln!("Split len != 4: {:?}", split);
        return Err(());
    }

    let track_nro = split[0].parse::<u16>().ok();
    let peak = match split[1].replace(',', ".").parse::<f32>() {
        Ok(res) => res,
        Err(_) => {
            eprintln!("Could not parse peak into f32: {}", split[1]);
            return Err(());
        }
    };

    Ok(TrackPeak {
        track_nro,
        peak,
        crc32: split[2].to_owned(),
        w_o_null: split[3].to_owned(),
    })
}

fn parse_ctdb_tocid(line: &str) -> Result<CtdbTocId, ()> {
    let split = line
        .split([':', ']'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() != 3 {
        return Err(());
    }

    let found = split[2] == "found.";

    Ok(CtdbTocId {
        id: split[1].to_owned(),
        found,
    })
}

fn parse_accurate_rip_id(line: &str) -> Result<AccurateRipId, ()> {
    let split = line
        .split([':', ']'])
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<&str>>();

    if split.len() < 3 {
        return Err(());
    }

    let found = split[2] == "found.";

    Ok(AccurateRipId {
        id: split[1].to_owned(),
        found,
    })
}

pub struct AccurateRipBuilder {
    fs_path: PathBuf,              // Path of the .accurip -file
    comment: Option<String>,       // Comment on the first line
    ctdb_tocid: Option<CtdbTocId>, //
    ctdbid_status: Vec<CtdbIdStatus>,
    ctdb_track_status: Vec<CtdbTrackStatus>,
    accuraterip_id: Option<AccurateRipId>,
    track_crc_v2_status: Vec<TrackStatusCrcV2>,
    track_offset_status: HashMap<i32, Vec<TrackOffsetStatus>>,
    track_peak: Vec<TrackPeak>,
}

impl AccurateRipBuilder {
    pub fn new(path: &Path) -> Self {
        AccurateRipBuilder {
            fs_path: path.to_path_buf(), // Path of the .accurip -file
            comment: None,               // Comment on the first line
            ctdb_tocid: None,
            ctdbid_status: Vec::<CtdbIdStatus>::new(),
            ctdb_track_status: Vec::<CtdbTrackStatus>::new(),
            accuraterip_id: None,
            track_crc_v2_status: Vec::<TrackStatusCrcV2>::new(),
            track_offset_status: HashMap::<i32, Vec<TrackOffsetStatus>>::new(),
            track_peak: Vec::<TrackPeak>::new(),
        }
    }

    pub fn comment(&mut self, comment: &str) -> &mut Self {
        self.comment = Some(comment.to_owned());
        self
    }

    pub fn ctdb_tocid(&mut self, ctdb_tocid: CtdbTocId) -> &mut Self {
        self.ctdb_tocid = Some(ctdb_tocid);
        self
    }

    pub fn ctdbid_status(&mut self, ctdbid_status: Vec<CtdbIdStatus>) -> &mut Self {
        self.ctdbid_status = ctdbid_status;
        self
    }

    pub fn ctdb_track_status(&mut self, ctdb_track_status: Vec<CtdbTrackStatus>) -> &mut Self {
        self.ctdb_track_status = ctdb_track_status;
        self
    }

    pub fn accuraterip_id(&mut self, accuraterip_id: AccurateRipId) -> &mut Self {
        self.accuraterip_id = Some(accuraterip_id);
        self
    }

    pub fn track_crc_v2_status(&mut self, track_crc_v2_status: Vec<TrackStatusCrcV2>) -> &mut Self {
        self.track_crc_v2_status = track_crc_v2_status;
        self
    }

    pub fn track_offset_status(
        &mut self,
        track_offset_status: HashMap<i32, Vec<TrackOffsetStatus>>,
    ) -> &mut Self {
        self.track_offset_status = track_offset_status;
        self
    }

    pub fn track_peak(&mut self, track_peak: Vec<TrackPeak>) -> &mut Self {
        self.track_peak = track_peak;
        self
    }

    pub fn build(&self) -> AccurateRip {
        AccurateRip {
            fs_path: self.fs_path.clone(),
            comment: self.comment.clone(),
            ctdb_tocid: self.ctdb_tocid.clone(),
            ctdbid_status: self.ctdbid_status.clone(),
            track_ctdb_status: self.ctdb_track_status.clone(),
            accuraterip_id: self.accuraterip_id.clone(),
            track_crc_v2_status: self.track_crc_v2_status.clone(),
            track_offset_status: self.track_offset_status.clone(),
            track_peak: self.track_peak.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    #![cfg_attr(rustfmt, rustfmt_skip)]
    use crate::music_db::accuraterip::*;

    const COMMENT_LINE: &str = "[CUETools log; Date: 09/01/2023 20:08:10; Version: 2.1.8]";
    const TOCID_LINE: &str = "[CTDB TOCID: Kdk0M8.6fdFeeQafgdEkP.A4Biw-] found.";
    const CTDBID_STATUS_HEADER: &str = "        [ CTDBID ] Status";
    const CTDBID_STATUS_LINE: &str = "        [ecb97790] (132/132) Accurately ripped";
    const TRACK_CTDB_STATUS_HEADER: &str = "Track | CTDB Status";
    const TRACK_CTDB_STATUS_LINE: &str = "  1   | (132/132) Accurately ripped";
    const ACCURATERIP_ID_LINE: &str = "[AccurateRip ID: 0006460d-002106ce-37061506] found.";
    const TRACK_CRC_V2_STATUS_HEADER: &str = "Track   [  CRC   |   V2   ] Status";
    const TRACK_CRC_V2_STATUS_LINE: &str = " 01     [aacf2d0c|9af097c8] (00+00/67) No match";
    const OFFSET_HEADER_1: &str = "Offsetted by -1858:";
    const OFFSET_LINE_1: &str = " 01     [38375132] (06/67) Accurately ripped";
    const OFFSET_HEADER_2: &str = "Offsetted by 755:";
    const OFFSET_LINE_2: &str = " 01     [4330fba3] (00/67) No match (V2 was not tested)";
    const TRACK_PEAK_HEADER: &str = "Track Peak [ CRC32  ] [W/O NULL] ";
    const TRACK_PEAK_LINE_1: &str = " --   96.2 [7EA791A7] [E7223B49]           ";
    const TRACK_PEAK_LINE_2: &str = " 01   96.2 [1B747A99] [D2BA13EA]           ";

    #[test]
    fn test_is_comment_line() {
        assert!(  is_comment_line(COMMENT_LINE) );
        assert!( !is_comment_line(TOCID_LINE) );
        assert!( !is_comment_line(CTDBID_STATUS_HEADER) );
        assert!( !is_comment_line(CTDBID_STATUS_LINE) );
        assert!( !is_comment_line(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_comment_line(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_comment_line(ACCURATERIP_ID_LINE) );
        assert!( !is_comment_line(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_comment_line(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_comment_line(OFFSET_HEADER_1) );
        assert!( !is_comment_line(OFFSET_LINE_1) );
        assert!( !is_comment_line(OFFSET_HEADER_2) );
        assert!( !is_comment_line(OFFSET_LINE_2) );
        assert!( !is_comment_line(TRACK_PEAK_HEADER) );
        assert!( !is_comment_line(TRACK_PEAK_LINE_1) );
        assert!( !is_comment_line(TRACK_PEAK_LINE_2) );
    }

    #[test]
    fn test_is_ctbid_status_header() {
        assert!( !is_ctbid_status_header(COMMENT_LINE) );
        assert!( !is_ctbid_status_header(TOCID_LINE) );
        assert!(  is_ctbid_status_header(CTDBID_STATUS_HEADER) );
        assert!( !is_ctbid_status_header(CTDBID_STATUS_LINE) );
        assert!( !is_ctbid_status_header(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_ctbid_status_header(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_ctbid_status_header(ACCURATERIP_ID_LINE) );
        assert!( !is_ctbid_status_header(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_ctbid_status_header(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_ctbid_status_header(OFFSET_HEADER_1) );
        assert!( !is_ctbid_status_header(OFFSET_LINE_1) );
        assert!( !is_ctbid_status_header(OFFSET_HEADER_2) );
        assert!( !is_ctbid_status_header(OFFSET_LINE_2) );
        assert!( !is_ctbid_status_header(TRACK_PEAK_HEADER) );
        assert!( !is_ctbid_status_header(TRACK_PEAK_LINE_1) );
        assert!( !is_ctbid_status_header(TRACK_PEAK_LINE_2) );
    }
    #[test]
    fn test_is_track_ctdb_status_header() {
        assert!( !is_track_ctdb_status_header(COMMENT_LINE) );
        assert!( !is_track_ctdb_status_header(TOCID_LINE) );
        assert!( !is_track_ctdb_status_header(CTDBID_STATUS_HEADER) );
        assert!( !is_track_ctdb_status_header(CTDBID_STATUS_LINE) );
        assert!(  is_track_ctdb_status_header(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_track_ctdb_status_header(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_track_ctdb_status_header(ACCURATERIP_ID_LINE) );
        assert!( !is_track_ctdb_status_header(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_track_ctdb_status_header(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_track_ctdb_status_header(OFFSET_HEADER_1) );
        assert!( !is_track_ctdb_status_header(OFFSET_LINE_1) );
        assert!( !is_track_ctdb_status_header(OFFSET_HEADER_2) );
        assert!( !is_track_ctdb_status_header(OFFSET_LINE_2) );
        assert!( !is_track_ctdb_status_header(TRACK_PEAK_HEADER) );
        assert!( !is_track_ctdb_status_header(TRACK_PEAK_LINE_1) );
        assert!( !is_track_ctdb_status_header(TRACK_PEAK_LINE_2) );
    }

    #[test]
    fn test_is_track_crc_v2_status_header() {
        assert!( !is_track_crc_v2_status_header(COMMENT_LINE) );
        assert!( !is_track_crc_v2_status_header(TOCID_LINE) );
        assert!( !is_track_crc_v2_status_header(CTDBID_STATUS_HEADER) );
        assert!( !is_track_crc_v2_status_header(CTDBID_STATUS_LINE) );
        assert!( !is_track_crc_v2_status_header(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_track_crc_v2_status_header(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_track_crc_v2_status_header(ACCURATERIP_ID_LINE) );
        assert!(  is_track_crc_v2_status_header(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_track_crc_v2_status_header(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_track_crc_v2_status_header(OFFSET_HEADER_1) );
        assert!( !is_track_crc_v2_status_header(OFFSET_LINE_1) );
        assert!( !is_track_crc_v2_status_header(OFFSET_HEADER_2) );
        assert!( !is_track_crc_v2_status_header(OFFSET_LINE_2) );
        assert!( !is_track_crc_v2_status_header(TRACK_PEAK_HEADER) );
        assert!( !is_track_crc_v2_status_header(TRACK_PEAK_LINE_1) );
        assert!( !is_track_crc_v2_status_header(TRACK_PEAK_LINE_2) );
    }

    #[test]
    fn test_is_offset_header() {
        assert!( !is_offset_header(COMMENT_LINE) );
        assert!( !is_offset_header(TOCID_LINE) );
        assert!( !is_offset_header(CTDBID_STATUS_HEADER) );
        assert!( !is_offset_header(CTDBID_STATUS_LINE) );
        assert!( !is_offset_header(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_offset_header(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_offset_header(ACCURATERIP_ID_LINE) );
        assert!( !is_offset_header(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_offset_header(TRACK_CRC_V2_STATUS_LINE) );
        assert!(  is_offset_header(OFFSET_HEADER_1) );
        assert!( !is_offset_header(OFFSET_LINE_1) );
        assert!(  is_offset_header(OFFSET_HEADER_2) );
        assert!( !is_offset_header(OFFSET_LINE_2) );
        assert!( !is_offset_header(TRACK_PEAK_HEADER) );
        assert!( !is_offset_header(TRACK_PEAK_LINE_1) );
        assert!( !is_offset_header(TRACK_PEAK_LINE_2) );
    }

    #[test]
    fn test_is_track_peak_header() {
        assert!( !is_track_peak_header(COMMENT_LINE) );
        assert!( !is_track_peak_header(TOCID_LINE) );
        assert!( !is_track_peak_header(CTDBID_STATUS_HEADER) );
        assert!( !is_track_peak_header(CTDBID_STATUS_LINE) );
        assert!( !is_track_peak_header(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_track_peak_header(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_track_peak_header(ACCURATERIP_ID_LINE) );
        assert!( !is_track_peak_header(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_track_peak_header(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_track_peak_header(OFFSET_HEADER_1) );
        assert!( !is_track_peak_header(OFFSET_LINE_1) );
        assert!( !is_track_peak_header(OFFSET_HEADER_2) );
        assert!( !is_track_peak_header(OFFSET_LINE_2) );
        assert!(  is_track_peak_header(TRACK_PEAK_HEADER) );
        assert!( !is_track_peak_header(TRACK_PEAK_LINE_1) );
        assert!( !is_track_peak_header(TRACK_PEAK_LINE_2) );
    }
    #[test]
    fn test_is_accuraterip_id_line() {
        assert!( !is_accuraterip_id_line(COMMENT_LINE) );
        assert!( !is_accuraterip_id_line(TOCID_LINE) );
        assert!( !is_accuraterip_id_line(CTDBID_STATUS_HEADER) );
        assert!( !is_accuraterip_id_line(CTDBID_STATUS_LINE) );
        assert!( !is_accuraterip_id_line(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_accuraterip_id_line(TRACK_CTDB_STATUS_LINE) );
        assert!(  is_accuraterip_id_line(ACCURATERIP_ID_LINE) );
        assert!( !is_accuraterip_id_line(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_accuraterip_id_line(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_accuraterip_id_line(OFFSET_HEADER_1) );
        assert!( !is_accuraterip_id_line(OFFSET_LINE_1) );
        assert!( !is_accuraterip_id_line(OFFSET_HEADER_2) );
        assert!( !is_accuraterip_id_line(OFFSET_LINE_2) );
        assert!( !is_accuraterip_id_line(TRACK_PEAK_HEADER) );
        assert!( !is_accuraterip_id_line(TRACK_PEAK_LINE_1) );
        assert!( !is_accuraterip_id_line(TRACK_PEAK_LINE_2) );
    }

    #[test]
    fn test_is_ctdb_tocid_line() {
        assert!( !is_ctdb_tocid_line(COMMENT_LINE) );
        assert!(  is_ctdb_tocid_line(TOCID_LINE) );
        assert!( !is_ctdb_tocid_line(CTDBID_STATUS_HEADER) );
        assert!( !is_ctdb_tocid_line(CTDBID_STATUS_LINE) );
        assert!( !is_ctdb_tocid_line(TRACK_CTDB_STATUS_HEADER) );
        assert!( !is_ctdb_tocid_line(TRACK_CTDB_STATUS_LINE) );
        assert!( !is_ctdb_tocid_line(ACCURATERIP_ID_LINE) );
        assert!( !is_ctdb_tocid_line(TRACK_CRC_V2_STATUS_HEADER) );
        assert!( !is_ctdb_tocid_line(TRACK_CRC_V2_STATUS_LINE) );
        assert!( !is_ctdb_tocid_line(OFFSET_HEADER_1) );
        assert!( !is_ctdb_tocid_line(OFFSET_LINE_1) );
        assert!( !is_ctdb_tocid_line(OFFSET_HEADER_2) );
        assert!( !is_ctdb_tocid_line(OFFSET_LINE_2) );
        assert!( !is_ctdb_tocid_line(TRACK_PEAK_HEADER) );
        assert!( !is_ctdb_tocid_line(TRACK_PEAK_LINE_1) );
        assert!( !is_ctdb_tocid_line(TRACK_PEAK_LINE_2) );
    }

    #[test]
    fn test_parse_offset_from_offset_header() {
        let ok_res1 = Ok(-1858);
        let ok_res2 = Ok(755);
        let err_res = Err(());
        assert_eq!( parse_offset_from_offset_header(COMMENT_LINE), err_res);
        assert_eq!( parse_offset_from_offset_header(TOCID_LINE), err_res);
        assert_eq!( parse_offset_from_offset_header(CTDBID_STATUS_HEADER), err_res);
        assert_eq!( parse_offset_from_offset_header(CTDBID_STATUS_LINE), err_res);
        assert_eq!( parse_offset_from_offset_header(TRACK_CTDB_STATUS_HEADER), err_res );
        assert_eq!( parse_offset_from_offset_header(TRACK_CTDB_STATUS_LINE), err_res );
        assert_eq!( parse_offset_from_offset_header(ACCURATERIP_ID_LINE), err_res );
        assert_eq!( parse_offset_from_offset_header(TRACK_CRC_V2_STATUS_HEADER), err_res);
        assert_eq!( parse_offset_from_offset_header(TRACK_CRC_V2_STATUS_LINE), err_res);
        assert_eq!( parse_offset_from_offset_header(OFFSET_HEADER_1), ok_res1);
        assert_eq!( parse_offset_from_offset_header(OFFSET_LINE_1), err_res);
        assert_eq!( parse_offset_from_offset_header(OFFSET_HEADER_2), ok_res2);
        assert_eq!( parse_offset_from_offset_header(OFFSET_LINE_2), err_res);
        assert_eq!( parse_offset_from_offset_header(TRACK_PEAK_HEADER), err_res);
        assert_eq!( parse_offset_from_offset_header(TRACK_PEAK_LINE_1), err_res);
        assert_eq!( parse_offset_from_offset_header(TRACK_PEAK_LINE_2), err_res);
    }
    #[test]
    fn test_parse_ctdbid_status_line () {
        let ok_res: Result<CtdbIdStatus, ()> = Ok(CtdbIdStatus {
            ctdbid: "ecb97790".to_owned(),
            status: (132, 132),
            msg: CtdbIdStatusMessage::AccuratelyRipped,
        });
        assert_eq!( parse_ctdbid_status_line(CTDBID_STATUS_LINE), ok_res);
    }
    #[test]
    fn test_parse_track_ctdb_status_line () {
        let ok_res: Result<CtdbTrackStatus, ()> = Ok(CtdbTrackStatus {
            track_nro: 1,
            status: (132, 132),
            msg: CtdbTrackStatusMessage::AccuratelyRipped,
        });
        assert_eq!( parse_track_ctdb_status_line(TRACK_CTDB_STATUS_LINE), ok_res);
    }
    #[test]
    fn test_parse_track_status_crc_v2_line () {
        let ok_res: Result<TrackStatusCrcV2, ()> = Ok(TrackStatusCrcV2 {
            track_nro: 1,
            crc: "aacf2d0c".to_owned(),
            crc_v2: "9af097c8".to_owned(),
            status: (0, 0, 67),
            msg: TrackStatusCrcV2Message::NoMatch,
        });
        assert_eq!( parse_track_status_crc_v2_line(TRACK_CRC_V2_STATUS_LINE), ok_res);
    }
    #[test]
    fn test_parse_track_offset_line () {
        let ok_res: Result<TrackOffsetStatus, ()> = Ok(TrackOffsetStatus {
            track_nro: 1,
            crc: "38375132".to_owned(),
            status: (6, 67),
            msg: TrackOffsetStatusMessage::AccuretelyRipped,
        });
        assert_eq!( parse_track_offset_line(OFFSET_LINE_1), ok_res);
    }
    #[test]
    fn test_parse_track_peak_line () {
        let ok_res_1: Result<TrackPeak, ()> = Ok(TrackPeak {
            track_nro: None,
            peak: 96.2,
            crc32: "7EA791A7".to_owned(),
            w_o_null: "E7223B49".to_owned(),
        });
        let ok_res_2: Result<TrackPeak, ()> = Ok(TrackPeak {
            track_nro: Some(1),
            peak: 96.2,
            crc32: "1B747A99".to_owned(),
            w_o_null: "D2BA13EA".to_owned(),
        });
        assert_eq!( parse_track_peak_line(TRACK_PEAK_LINE_1), ok_res_1);
        assert_eq!( parse_track_peak_line(TRACK_PEAK_LINE_2), ok_res_2);
    }
    #[test]
    fn test_parse_ctdb_tocid () {
        let ok_res: Result<CtdbTocId, ()> = Ok(CtdbTocId {
            id: "Kdk0M8.6fdFeeQafgdEkP.A4Biw-".to_owned(),
            found: true
        });
        assert_eq!( parse_ctdb_tocid(TOCID_LINE), ok_res);
    }
    #[test]
    fn test_parse_accurate_rip_id () {
        let ok_res: Result<AccurateRipId, ()> = Ok(AccurateRipId {
            id: "0006460d-002106ce-37061506".to_owned(),
            found: true,
        });
        assert_eq!( parse_accurate_rip_id(ACCURATERIP_ID_LINE), ok_res);
    }
}
