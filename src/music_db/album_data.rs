use core::mem::size_of_val;
use std::fmt;
use std::path::Path;
use std::path::PathBuf;

use crate::{music_db::music_db_error::MusicDbError, util};

pub struct AlbumData {
    fs_path: PathBuf,
    artist: Option<String>, // Artist name based on dir name
    name: Option<String>,   // Album name based on dir
    year: Option<u16>,      // Album year based on dir
}

impl AlbumData {
    #[allow(dead_code)]
    pub fn builder(path: &Path) -> AlbumDataBuilder {
        AlbumDataBuilder::new(path)
    }

    /// Construct a new album based on a directory on filesystem
    pub fn try_from_path(path: &Path) -> Result<Self, MusicDbError> {
        let rce = MusicDbError::AlbumParse(path.to_path_buf());

        if !path.exists() || !path.is_dir() {
            return Err(rce);
        }

        let mut adb = AlbumDataBuilder::new(path);

        let album_name_split: Vec<&str> = path
            .file_name()
            .ok_or(rce.clone())?
            .to_str()
            .ok_or(rce)?
            .split(util::ALBUM_DELIMETER)
            .collect();

        if album_name_split.len() == 3 {
            adb.artist(album_name_split[0]);
            adb.album(album_name_split[1]);
            if let Ok(y) = album_name_split[2].parse::<u16>() {
                adb.year(y);
            }
        }

        Ok(adb.build())
    }

    pub fn path(&self) -> PathBuf {
        self.fs_path.to_owned()
    }

    pub fn artist(&self) -> Result<String, MusicDbError> {
        self.artist
            .clone()
            .ok_or(MusicDbError::AlbumArtistNotDefined(
                self.fs_path.to_path_buf(),
            ))
    }

    pub fn album(&self) -> Result<String, MusicDbError> {
        self.name.clone().ok_or(MusicDbError::AlbumNameNotDefined(
            self.fs_path.to_path_buf(),
        ))
    }

    #[allow(dead_code)]
    pub fn year(&self) -> Result<u16, MusicDbError> {
        self.year.ok_or(MusicDbError::AlbumYearNotDefined(
            self.fs_path.to_path_buf(),
        ))
    }

    pub fn size_in_mem(&self) -> usize {
        let mut total_mem: usize = 0;
        let size_of_fs_path = size_of_val(&*self.fs_path);
        total_mem += size_of_fs_path;

        if let Some(artist_name) = &self.artist {
            let size_of_artist = size_of_val(&**artist_name);
            total_mem += size_of_artist;
        }
        if let Some(album_name) = &self.name {
            let size_of_name = size_of_val(&**album_name);
            total_mem += size_of_name;
        }
        let size_of_self = size_of_val(self);
        total_mem += size_of_self;
        total_mem
    }
}

impl fmt::Display for AlbumData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let artist: String = match &self.artist {
            Some(name) => name.to_string(),
            None => String::from("<unknown artist>"),
        };
        let album: String = match &self.name {
            Some(name) => name.to_string(),
            None => String::from("<unknown album>"),
        };
        let year: String = match self.year {
            Some(y) => y.to_string(),
            None => String::from("<unknown year>"),
        };
        write!(f, "{} - {} - {}", artist, album, year)
    }
}

pub struct AlbumDataBuilder {
    fs_path: PathBuf,
    artist: Option<String>, // Artist name based on dir name
    album: Option<String>,  // Album name based on dir
    year: Option<u16>,      // Album year based on dir
}

impl AlbumDataBuilder {
    pub fn new(path: &Path) -> Self {
        AlbumDataBuilder {
            fs_path: path.to_path_buf(),
            artist: None,
            album: None,
            year: None,
        }
    }

    pub fn artist(&mut self, artist: &str) -> &mut Self {
        self.artist = Some(artist.to_owned());
        self
    }

    pub fn album(&mut self, album: &str) -> &mut Self {
        self.album = Some(album.to_owned());
        self
    }

    pub fn year(&mut self, year: u16) -> &mut Self {
        self.year = Some(year);
        self
    }

    pub fn build(&self) -> AlbumData {
        AlbumData {
            fs_path: self.fs_path.clone(),
            artist: self.artist.clone(),
            name: self.album.clone(),
            year: self.year,
        }
    }
}
