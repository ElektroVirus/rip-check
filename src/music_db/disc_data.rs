use crate::{
    music_db::music_db_error::MusicDbError,
    music_db::{
        accuraterip::AccurateRip,
        disc_filetype::{disc_filetype, DiscFileType},
        trackdata::TrackData,
    },
};

use core::mem::size_of_val;
use fs_err as fs;
use std::fmt;
use std::io;
use std::path::Path;
use std::path::PathBuf;

pub struct DiscData {
    fs_path: PathBuf,
    disc_name: Option<String>, // CD name based on dir
    /* If CD is in album root, disc_nro == 1. If multiple CDs, first CD's disc_nro == 1 */
    disc_nro: u32,
    tracks: Vec<TrackData>,
    accurip: Option<Box<AccurateRip>>,
    cuesheet: Option<PathBuf>,
    riplog: Option<PathBuf>,
    playlist: Option<PathBuf>,
    albumart: Vec<PathBuf>,
    unknown_files: Vec<PathBuf>,
}

impl fmt::Display for DiscData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let disc_name: String = match &self.disc_name {
            Some(name) => name.to_string(),
            None => String::from("<unknown disc>"),
        };
        write!(f, "{}", disc_name)
    }
}

impl DiscData {
    #[allow(dead_code)]
    pub fn builder(path: &Path) -> DiscDataBuilder {
        DiscDataBuilder::new(path)
    }

    pub fn try_from_path(
        path: &Path,
        disc_name: Option<&str>,
        disc_nro: u32,
    ) -> Result<Self, MusicDbError> {
        if !path.exists() || !path.is_dir() || !Self::dir_is_disc(path) {
            return Err(MusicDbError::DiscParse(path.to_path_buf()));
        }

        let mut ddb = DiscDataBuilder::new(path);
        if let Some(disc_name) = disc_name {
            ddb.disc_name(disc_name);
        }
        ddb.disc_nro(disc_nro);

        let mut tracks: Vec<TrackData> = Vec::new();
        for path in fs::read_dir(path)?.flatten() {
            let path = path.path();
            match disc_filetype(&path)? {
                DiscFileType::Audio => {
                    if let Ok(t) = TrackData::try_from_path(&path) {
                        tracks.push(t);
                    }
                }
                DiscFileType::Accurip => {
                    if let Ok(ar) = AccurateRip::try_from_path(&path) {
                        ddb.accurip(ar);
                    }
                }
                DiscFileType::CueSheet => {
                    ddb.cuesheet(&path);
                }
                DiscFileType::Log => {
                    ddb.riplog(&path);
                }
                DiscFileType::Playlist => {
                    ddb.playlist(&path);
                }
                DiscFileType::AlbumArt => {
                    ddb.albumart(&path);
                }
                DiscFileType::Unknown => {
                    ddb.unknown_file(&path);
                }
            }
        }

        ddb.tracks(tracks);

        Ok(ddb.build())
    }

    pub fn fs_path(&self) -> PathBuf {
        self.fs_path.to_owned()
    }

    // TODO: This is bad API. It's quite possible, that the disk does not have
    // a name.
    pub fn name(&self) -> Result<String, MusicDbError> {
        self.disc_name
            .clone()
            .ok_or(MusicDbError::DiscNameNotDefined(self.fs_path.to_path_buf()))
    }

    pub fn disc_nro(&self) -> u32 {
        self.disc_nro
    }

    pub fn tracks(&self) -> &Vec<TrackData> {
        &self.tracks
    }

    pub fn nro_of_tracks(&self) -> usize {
        self.tracks.len()
    }

    pub fn accuraterip(&self) -> Option<Box<AccurateRip>> {
        self.accurip.clone()
    }

    /// This function iterates over files in directory and counts audio files.
    /// This function will not
    /// - check the song number and compare it with the total number of songs.
    /// - check the song number in metadata. It will be done in metadatacheck
    fn count_audiofiles(discpath: &Path) -> Result<u32, io::Error> {
        let mut nro_of_audiofiles: u32 = 0;
        for path in fs::read_dir(discpath)?.flatten() {
            if let Ok(DiscFileType::Audio) = disc_filetype(&path.path()) {
                nro_of_audiofiles += 1
            }
        }
        Ok(nro_of_audiofiles)
    }

    /// This function will tell, if a directory is a CD or not.
    /// At the moment this function returns true if the directory contains
    /// at least one audio file.
    fn dir_is_disc(albumpath: &Path) -> bool {
        if let Ok(nro) = Self::count_audiofiles(albumpath) {
            if nro > 0 {
                return true;
            }
        }
        false
    }

    pub fn size_in_mem(&self) -> usize {
        let mut total_mem: usize = 0;
        let size_of_fs_path = size_of_val(&*self.fs_path);
        total_mem += size_of_fs_path;

        if let Some(disc_name) = &self.disc_name {
            let size_of_disc_name = size_of_val(&**disc_name);
            total_mem += size_of_disc_name;
        }

        let size_of_tracks = size_of_val(&*self.tracks);
        total_mem += size_of_tracks;

        if let Some(ac) = &self.accurip {
            let size_of_ac = size_of_val(ac);
            total_mem += size_of_ac;
        }

        if let Some(cs) = &self.cuesheet {
            let size_of_cs = size_of_val(&**cs);
            total_mem += size_of_cs;
        }

        if let Some(rl) = &self.riplog {
            let size_of_rl = size_of_val(&**rl);
            total_mem += size_of_rl;
        }

        if let Some(pl) = &self.playlist {
            let size_of_pl = size_of_val(&**pl);
            total_mem += size_of_pl;
        }

        let size_of_albumart = size_of_val(&*self.albumart);
        total_mem += size_of_albumart;

        let size_of_other_files = size_of_val(&*self.unknown_files);
        total_mem += size_of_other_files;

        let size_of_self = size_of_val(self);
        total_mem += size_of_self;
        total_mem
    }
}

pub struct DiscDataBuilder {
    fs_path: PathBuf,
    disc_name: Option<String>,
    disc_nro: u32,
    tracks: Vec<TrackData>,
    accurip: Option<Box<AccurateRip>>,
    cuesheet: Option<PathBuf>,
    riplog: Option<PathBuf>,
    playlist: Option<PathBuf>,
    albumart: Vec<PathBuf>,
    unknown_files: Vec<PathBuf>,
}

impl DiscDataBuilder {
    pub fn new(path: &Path) -> Self {
        DiscDataBuilder {
            fs_path: path.to_path_buf(),
            disc_name: None,
            disc_nro: 0,
            tracks: Vec::new(),
            accurip: None,
            cuesheet: None,
            riplog: None,
            playlist: None,
            albumart: Vec::new(),
            unknown_files: Vec::new(),
        }
    }

    pub fn disc_name(&mut self, disc_name: &str) -> &mut Self {
        self.disc_name = Some(disc_name.to_owned());
        self
    }

    pub fn disc_nro(&mut self, disc_nro: u32) -> &mut Self {
        self.disc_nro = disc_nro;
        self
    }

    pub fn tracks(&mut self, tracks: Vec<TrackData>) -> &mut Self {
        self.tracks = tracks;
        self
    }

    pub fn accurip(&mut self, ar: AccurateRip) -> &mut Self {
        self.accurip = Some(Box::new(ar));
        self
    }

    pub fn cuesheet(&mut self, cs: &Path) -> &mut Self {
        self.cuesheet = Some(cs.to_path_buf());
        self
    }

    pub fn riplog(&mut self, rl: &Path) -> &mut Self {
        self.riplog = Some(rl.to_path_buf());
        self
    }

    pub fn playlist(&mut self, pl: &Path) -> &mut Self {
        self.playlist = Some(pl.to_path_buf());
        self
    }

    pub fn albumart(&mut self, aa: &Path) -> &mut Self {
        self.albumart.push(aa.to_path_buf());
        self
    }

    pub fn unknown_file(&mut self, of: &Path) -> &mut Self {
        self.unknown_files.push(of.to_path_buf());
        self
    }

    pub fn build(&self) -> DiscData {
        DiscData {
            fs_path: self.fs_path.clone(),
            disc_name: self.disc_name.clone(),
            disc_nro: self.disc_nro,
            tracks: self.tracks.clone(),
            accurip: self.accurip.clone(),
            cuesheet: self.cuesheet.clone(),
            riplog: self.riplog.clone(),
            playlist: self.playlist.clone(),
            albumart: self.albumart.clone(),
            unknown_files: self.unknown_files.clone(),
        }
    }
}
