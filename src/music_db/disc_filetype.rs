use crate::music_db::music_db_error::MusicDbError;
use std::path::Path;

pub enum DiscFileType {
    Audio,
    Accurip,
    CueSheet,
    Log,
    Playlist,
    AlbumArt,
    Unknown,
}

pub fn disc_filetype(path: &Path) -> Result<DiscFileType, MusicDbError> {
    let extension = path
        .extension()
        .ok_or(MusicDbError::DiscFiletypeError(path.to_path_buf()))?
        .to_str()
        .ok_or(MusicDbError::DiscFiletypeError(path.to_path_buf()))?
        .to_string();

    // TODO: This list is not comprehensive
    if extension == "flac"
        || extension == "opus"
        || extension == "ogg"
        || extension == "wav"
        || extension == "wma"
        || extension == "aac"
        || extension == "mp3"
    {
        return Ok(DiscFileType::Audio);
    } else if extension == "accurip" {
        return Ok(DiscFileType::Accurip);
    } else if extension == "cue" {
        return Ok(DiscFileType::CueSheet);
    } else if extension == "log" {
        return Ok(DiscFileType::Log);
    } else if extension == "m3u" {
        return Ok(DiscFileType::Playlist);
    } else if extension == "jpg" || extension == "png" {
        return Ok(DiscFileType::AlbumArt);
    }

    Ok(DiscFileType::Unknown)
}
