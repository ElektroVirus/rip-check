use crate::{
    music_db::album_data::AlbumData, music_db::disc_data::DiscData,
    music_db::music_db_error::MusicDbError,
};

use fs_err as fs;
use regex::Regex;
use std::fmt;
use std::io;
use std::path::Path;
use std::path::PathBuf;

#[allow(clippy::enum_variant_names)]
pub enum Entry {
    SingleDiscAlbum(AlbumData, DiscData),
    ManyDiscAlbum(AlbumData, Vec<DiscData>),
    ManyDiscAlbumWithRoot(AlbumData, Vec<DiscData>),
    NoneDiscAlbum(AlbumData),
}

impl Entry {
    pub fn entry_from_dir(path: &Path) -> Result<Entry, MusicDbError> {
        let ad = match AlbumData::try_from_path(path) {
            Ok(ad) => ad,
            Err(_) => return Err(MusicDbError::EntryPathNotFoundError(path.to_path_buf())),
        };

        // Check if album dir contains subdirectories for CDs
        let mut ddv: Vec<DiscData> = check_album_for_disc_subdirs(path)?;

        // Check, if album contains a disc in its root. It's number will be 1
        match DiscData::try_from_path(path, None, 1) {
            // There is a CD directly in the root of the album
            Ok(disc_root) => {
                if ddv.is_empty() {
                    // Simplest case: album contains only one CD
                    Ok(Entry::SingleDiscAlbum(ad, disc_root))
                } else {
                    // Most complicated case: there are audio files in the
                    // root but also in subdirs
                    //
                    // TODO: this is not allowed, multiple CDs must all be
                    // in their own subdirectories. Only a single CD
                    // must directly be in the root of the album.
                    ddv.insert(0, disc_root);
                    Ok(Entry::ManyDiscAlbumWithRoot(ad, ddv))
                }
            }
            // No CD in album root
            Err(disc_parse_error) => {
                eprintln!("Parsing CD root failed: {}", disc_parse_error);
                if ddv.is_empty() {
                    // Case: Neither audio files in root, nor do subdirs exist
                    //
                    // TODO: This is not allowed. Every album has at least 1 CD.
                    Ok(Entry::NoneDiscAlbum(ad))
                } else {
                    // Case: no audio files in root, subdirs exist.
                    Ok(Entry::ManyDiscAlbum(ad, ddv))
                }
            }
        }
    }

    pub fn nro_of_discs(&self) -> usize {
        match self {
            Entry::SingleDiscAlbum(_ad, _dd) => 1,
            Entry::ManyDiscAlbum(_ad, ddv) => ddv.len(),
            Entry::ManyDiscAlbumWithRoot(_ad, ddv) => ddv.len(),
            Entry::NoneDiscAlbum(_ad) => 0,
        }
    }

    pub fn size_in_mem(&self) -> usize {
        match self {
            Entry::SingleDiscAlbum(ad, dd) => ad.size_in_mem() + dd.size_in_mem(),
            Entry::ManyDiscAlbum(ad, ddv) => {
                let mut total_mem: usize = 0;
                for dd in ddv {
                    total_mem += dd.size_in_mem();
                }
                total_mem + ad.size_in_mem()
            }
            Entry::ManyDiscAlbumWithRoot(ad, ddv) => {
                let mut total_mem: usize = 0;
                for dd in ddv {
                    total_mem += dd.size_in_mem();
                }
                total_mem + ad.size_in_mem()
            }
            Entry::NoneDiscAlbum(ad) => ad.size_in_mem(),
        }
    }

    pub fn fs_path(&self) -> PathBuf {
        match self {
            Entry::SingleDiscAlbum(ad, _dd) => ad.path(),
            Entry::ManyDiscAlbum(ad, _ddv) => ad.path(),
            Entry::ManyDiscAlbumWithRoot(ad, _ddv) => ad.path(),
            Entry::NoneDiscAlbum(ad) => ad.path(),
        }
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Entry ")?;
        match self {
            Entry::SingleDiscAlbum(ad, _dd) => write!(f, "SingleDiscAlbum: {}", ad)?,
            Entry::ManyDiscAlbum(ad, ddv) => {
                write!(f, "ManyDiscAlbum: {}", ad)?;
                for dd in ddv {
                    writeln!(f)?;
                    write!(f, "    {}", dd)?;
                }
                return Ok(());
            }
            Entry::ManyDiscAlbumWithRoot(ad, ddv) => {
                write!(f, "ManyDiscAlbumWithRoot: {}", ad)?;
                for dd in ddv {
                    writeln!(f)?;
                    write!(f, "    {}", dd)?;
                }
                return Ok(());
            }
            Entry::NoneDiscAlbum(ad) => write!(f, "NoneDiscAlbum: {}", ad)?,
        }
        Ok(())
    }
}

impl fmt::Debug for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Entry ")?;
        match self {
            Entry::SingleDiscAlbum(ad, dd) => {
                writeln!(f, "SingleDiscAlbum: {}", ad)?;
                writeln!(f, "  AlbumData size: {} bytes", ad.size_in_mem())?;
                write!(f, "  DiscData size:    {} bytes", dd.size_in_mem())?;
                return Ok(());
            }
            Entry::ManyDiscAlbum(ad, ddv) => {
                write!(f, "ManyDiscAlbum: {}", ad)?;
                for dd in ddv {
                    writeln!(f)?;
                    write!(f, "    {}", dd)?;
                }
                return Ok(());
            }
            Entry::ManyDiscAlbumWithRoot(ad, ddv) => {
                write!(f, "ManyDiscAlbumWithRoot: {}", ad)?;
                for dd in ddv {
                    writeln!(f)?;
                    write!(f, "    {}", dd)?;
                }
                return Ok(());
            }
            Entry::NoneDiscAlbum(ad) => write!(f, "NoneDiscAlbum: {}", ad)?,
        }
        Ok(())
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct DiscSubdir {
    path: PathBuf,
    disc_name: String,
}

use thiserror::Error;

#[derive(Error, Debug, Clone)]
pub enum CheckAlbumError {
    #[error("regex error: {0:?}")]
    RegexError(#[from] regex::Error),
    #[error("stdio error: {0:?}")]
    StdIoError(io::ErrorKind),
}

impl From<io::Error> for CheckAlbumError {
    fn from(value: io::Error) -> Self {
        Self::StdIoError(value.kind())
    }
}

/// Check for subdirectories in album (e.g. if it contains multiple discs).
///
/// Returns list of paths to subdirs named
/// CD[0-9]+
///
/// If there exists a directory named CD2, there must also be a directory called CD1.
/// etc. This function will check that and report errors for missing directories.
/// TODO: This could also directly return CD strucsts
fn check_album_for_disc_subdirs(albumpath: &Path) -> Result<Vec<DiscData>, CheckAlbumError> {
    let mut subdirs: Vec<DiscSubdir> = Vec::new();

    let re = Regex::new(r"CD\d+$")?;

    let _albumpath_str = albumpath.to_str().unwrap();

    // Iterate over files in dir
    for path in fs::read_dir(albumpath).unwrap() {
        let path = path?;
        let fname = path.file_name().to_str().unwrap().to_string();
        let _fpath = path.path().into_os_string().into_string().unwrap();

        // Check whether path is a directory or a file
        if path.path().is_file() {
            // A file does not interest in this function
            continue;
        } else if path.path().is_dir() {
            // Check directory name against regex whether it is valid subdir name for us
            if let Some(_mat) = re.find(&fname) {
                subdirs.push(DiscSubdir {
                    path: path.path(),
                    disc_name: fname,
                });
            } else {
                let err_msg = format!("Illegal subdir name {}", fname);
                eprintln!("{}", err_msg);
            }
        }
    }

    // Sort relevant subdirs so that the traversal is predictable
    subdirs.sort();
    Ok(create_discdata(&subdirs))
}

fn create_discdata(discsubdirs: &[DiscSubdir]) -> Vec<DiscData> {
    let mut res = Vec::<DiscData>::new();
    for (i, discsubdir) in discsubdirs.iter().enumerate() {
        if let Ok(dd) = DiscData::try_from_path(
            &discsubdir.path,
            Some(&discsubdir.disc_name),
            (i + 1) as u32,
        )
        // First CD number if 1
        {
            res.push(dd);
        }
    }
    res
}
