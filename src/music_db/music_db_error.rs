use std::{io, path::PathBuf};
use thiserror::Error;

use super::entry::CheckAlbumError;

#[derive(Error, Debug, Clone)]
pub enum MusicDbError {
    #[error("Parsing album failed at {0}")]
    AlbumParse(PathBuf),
    #[error("Album does not have a artist defined at {0}")]
    AlbumArtistNotDefined(PathBuf),
    #[error("Album does not have a name defined at {0}")]
    AlbumNameNotDefined(PathBuf),
    #[error("Album does not have a year defined at {0}")]
    AlbumYearNotDefined(PathBuf),
    #[error("Parsing disc failed at {0}")]
    DiscParse(PathBuf),
    #[error("Disc does not have a name defined at {0}")]
    DiscNameNotDefined(PathBuf),
    #[error("Track is not a recognized audio file at {0}")]
    TrackNotAnAudiofile(PathBuf),
    #[error("Track does not have a title defined at {0}")]
    TrackTitleNotDefined(PathBuf),
    #[error("Track does not have a number defined at {0}")]
    TrackNumberNotDefined(PathBuf),
    #[error("Track path invalid: {0}")]
    TrackPathError(PathBuf),
    #[error("Entry path not found: {0}")]
    EntryPathNotFoundError(PathBuf),
    #[error("CD path invalid: {0}")]
    DiscFiletypeError(PathBuf),
    #[error("Music database not found at {0}")]
    MusicDbNotFound(PathBuf),
    #[error("Logfile not defined")]
    LogfileNotDefined,
    #[error("stdio error: {0}")]
    StdIoError(io::ErrorKind),
    #[error("failed to check album: {0}")]
    CheckAlbumError(#[from] CheckAlbumError),
    #[error("Accurip file path not found: {0}")]
    AccuripPathNotFound(PathBuf),
    #[error("Accurip file could not be parsed at: {0}")]
    AccuripParseError(String, PathBuf),
}

impl From<io::Error> for MusicDbError {
    fn from(value: io::Error) -> Self {
        Self::StdIoError(value.kind())
    }
}
