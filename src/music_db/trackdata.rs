use crate::{
    music_db::disc_filetype::{disc_filetype, DiscFileType},
    music_db::music_db_error::MusicDbError,
    util,
};
use core::mem::size_of_val;
use std::{
    fmt,
    path::{Path, PathBuf},
};

#[derive(Clone)]
pub struct TrackData {
    fs_path: PathBuf,
    number: Option<u16>,
    title: Option<String>,
    artist: Option<String>,
}

impl TrackData {
    #[allow(dead_code)]
    pub fn builder(path: &Path) -> TrackDataBuilder {
        TrackDataBuilder::new(path)
    }

    /// Create a Track struct from filesystem path
    ///
    /// Filename should be of type
    /// <track_nro> - <artist> - <name>.flac
    /// e.g. '01 - Ensiferum - Dragonheads.flac'
    pub fn try_from_path(path: &Path) -> Result<Self, MusicDbError> {
        let fname: String = path
            .file_name()
            .ok_or(MusicDbError::TrackPathError(path.to_path_buf()))?
            .to_str()
            .ok_or(MusicDbError::TrackPathError(path.to_path_buf()))?
            .to_string();

        // Check if file is audio file. If not, track can not be constructed
        match disc_filetype(path)? {
            DiscFileType::Audio => {}
            _ => {
                return Err(MusicDbError::TrackNotAnAudiofile(path.to_path_buf()));
            }
        }

        let mut tb = TrackDataBuilder::new(path);

        let split = fname.split(util::TRACK_DELIMETER).collect::<Vec<&str>>();

        // If track name can be split as expected, fill in fields. Else
        // just construct a track without these fields
        if split.len() == 3 {
            if let Ok(track_nro) = split[0].parse::<u16>() {
                tb.number(track_nro);
            }

            let track_name: &str;
            if let Some((fname, _ftype)) = split[2].rsplit_once('.') {
                track_name = fname;
            } else {
                track_name = split[2];
            }
            tb.title(track_name);
            tb.artist(split[1]);
        }

        Ok(tb.build())
    }

    pub fn path(&self) -> &PathBuf {
        &self.fs_path
    }

    #[allow(dead_code)]
    pub fn number(&self) -> Result<u16, MusicDbError> {
        self.number.ok_or(MusicDbError::TrackNumberNotDefined(
            self.fs_path.to_path_buf(),
        ))
    }

    pub fn title(&self) -> Result<String, MusicDbError> {
        self.title.clone().ok_or(MusicDbError::TrackTitleNotDefined(
            self.fs_path.to_path_buf(),
        ))
    }

    pub fn artist(&self) -> Result<String, MusicDbError> {
        self.artist
            .clone()
            .ok_or(MusicDbError::TrackTitleNotDefined(
                self.fs_path.to_path_buf(),
            ))
    }

    #[allow(dead_code)]
    pub fn size_in_mem(&self) -> usize {
        let mut total_mem: usize = 0;
        let size_of_fs_path = size_of_val(&*self.fs_path);
        total_mem += size_of_fs_path;

        if let Some(track_name) = &self.title {
            let size_of_track_name = size_of_val(&**track_name);
            total_mem += size_of_track_name;
        }
        if let Some(artist_name) = &self.title {
            let size_of_artist_name = size_of_val(&**artist_name);
            total_mem += size_of_artist_name;
        }
        let size_of_self = size_of_val(self);
        total_mem += size_of_self;
        total_mem
    }
}

impl fmt::Display for TrackData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let nro: String = match &self.number {
            Some(nro) => nro.to_string(),
            None => String::from("<unknown number>"),
        };
        // let nro = self.number.unwrap_or("sdf").to_string();
        let name: &str = self.title.as_deref().unwrap_or("<unknown name>");
        let artist: &str = self.title.as_deref().unwrap_or("<unknown artist>");
        write!(f, "{} - {} - {}", nro, artist, name)
    }
}

pub struct TrackDataBuilder {
    fs_path: PathBuf,
    number: Option<u16>,
    name: Option<String>,
    artist: Option<String>,
}

impl TrackDataBuilder {
    pub fn new(path: &Path) -> Self {
        TrackDataBuilder {
            fs_path: path.to_path_buf(),
            number: None,
            name: None,
            artist: None,
        }
    }

    pub fn number(&mut self, number: u16) -> &mut Self {
        self.number = Some(number);
        self
    }

    pub fn title(&mut self, name: &str) -> &mut Self {
        self.name = Some(name.to_owned());
        self
    }

    pub fn artist(&mut self, artist: &str) -> &mut Self {
        self.artist = Some(artist.to_owned());
        self
    }

    pub fn build(&self) -> TrackData {
        TrackData {
            fs_path: self.fs_path.clone(),
            number: self.number,
            title: self.name.clone(),
            artist: self.artist.clone(),
        }
    }
}
