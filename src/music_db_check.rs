use crate::{
    music_db::entry::Entry,
    music_db::MusicDb,
    music_db_check::check_accuraterip::check_accuraterip,
    music_db_check::check_disc_numbering::check_entry_disc_numbering,
    music_db_check::check_metadata::check_metadata,
    music_db_check::external_checks::run_external_checks_for_entry,
    ripcheck_logger::{RipCheckLogger, RipCheckLoggerMessage},
};
use std::{io, path::Path};

mod check_accuraterip;
mod check_disc_numbering;
mod check_metadata;
pub mod cueripper_metadata;
mod external_checks;

pub fn check_db(
    mdb: &MusicDb,
    external_checks_dir: &Path,
    quiet: bool,
    dry_run: bool,
) -> Result<(), io::Error> {
    for entry in mdb.db() {
        check_entry(entry, external_checks_dir, quiet, dry_run)?;
    }
    Ok(())
}

fn check_entry(
    entry: &Entry,
    external_checks_dir: &Path,
    quiet: bool,
    dry_run: bool,
) -> Result<(), io::Error> {
    // Logger will be used once for every entry
    let mut rclogger = RipCheckLogger::builder()
        .log_into_tty(!quiet)
        .log_into_file(!dry_run)
        .album_path(entry.fs_path())
        .color_tty(true)
        .build();

    check_album_type(entry, &mut rclogger)?;
    check_entry_disc_numbering(entry, &mut rclogger)?;
    check_metadata(entry, &mut rclogger)?;

    run_external_checks_for_entry(entry, external_checks_dir, &mut rclogger)?;

    check_accuraterip(entry, &mut rclogger)?;
    Ok(())
}

fn check_album_type(entry: &Entry, rclogger: &mut RipCheckLogger) -> Result<(), io::Error> {
    match entry {
        Entry::SingleDiscAlbum(_ad, _dd) => {}
        Entry::ManyDiscAlbum(_ad, _ddv) => {}
        Entry::ManyDiscAlbumWithRoot(_ad, _ddv) => {
            rclogger.log(&RipCheckLoggerMessage::Error(
                "music_db_check::check_album_type",
                "Album contains multiple CDs but still contains one CD in its root.",
            ))?;
        }
        Entry::NoneDiscAlbum(_ad) => {
            rclogger.log(&RipCheckLoggerMessage::Error(
                "music_db_check::check_album_type",
                "Album does not contain any CDs.",
            ))?;
        }
    }
    Ok(())
}
