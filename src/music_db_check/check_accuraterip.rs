use crate::music_db::album_data::AlbumData;
use crate::music_db::disc_data::DiscData;
use crate::{
    music_db::entry::Entry, ripcheck_logger::RipCheckLogger, ripcheck_logger::RipCheckLoggerMessage,
};
use std::io;

pub fn check_accuraterip(entry: &Entry, rclogger: &mut RipCheckLogger) -> Result<bool, io::Error> {
    match entry {
        Entry::SingleDiscAlbum(ad, dd) => check_discdata(entry, ad, dd, rclogger)?,
        Entry::ManyDiscAlbum(ad, ddv) => check_discdatas(entry, ad, ddv, rclogger)?,
        Entry::ManyDiscAlbumWithRoot(ad, ddv) => check_discdatas(entry, ad, ddv, rclogger)?,
        Entry::NoneDiscAlbum(ad) => {
            rclogger.log(&RipCheckLoggerMessage::Info(
                "check_accuraterip::check_accuraterip",
                format!("skipping NoneDiscAlbum {}", ad),
            ))?;
        }
    };
    Ok(true)
}

pub fn check_discdatas(
    entry: &Entry,
    ad: &AlbumData,
    ddv: &Vec<DiscData>,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    for dd in ddv {
        check_discdata(entry, ad, dd, rclogger)?
    }
    Ok(())
}

pub fn check_discdata(
    _entry: &Entry,
    _ad: &AlbumData,
    dd: &DiscData,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    if let Some(_accurip) = dd.accuraterip() {
        let info_msg = if let Ok(disk_name) = dd.name() {
            format!("AccuRip file found in disc {} ", disk_name)
        } else {
            "AccuRip file found".to_owned()
        };
        rclogger.log(&RipCheckLoggerMessage::Info(
            "check_accuraterip::check_discdata",
            info_msg,
        ))?;
    } else {
        let err_msg = if let Ok(disk_name) = dd.name() {
            format!(
                "Disc {} does not have a AccurateRip logfile .accurip or parsing it failed.",
                disk_name
            )
        } else {
            "Disc does not have a AccurateRip logfile .accurip or parsing it failed.".to_owned()
        };
        rclogger.log(&RipCheckLoggerMessage::Error(
            "check_accuraterip::check_discdata",
            err_msg,
        ))?;
    }
    Ok(())
}
