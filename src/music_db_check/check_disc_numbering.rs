use crate::{
    music_db::album_data::AlbumData,
    music_db::disc_data::DiscData,
    music_db::entry::Entry,
    ripcheck_logger::{RipCheckLogger, RipCheckLoggerMessage},
};
use std::io;

pub fn check_entry_disc_numbering(
    entry: &Entry,
    rclogger: &mut RipCheckLogger,
) -> Result<bool, io::Error> {
    fn check_disc_numbering(_ad: &AlbumData, ddv: &[DiscData], disc_number_offset: usize) -> bool {
        for (i, dd) in ddv.iter().enumerate() {
            let expected_dirname = format!("CD{}", i + disc_number_offset);
            if let Ok(disc_name) = dd.name() {
                if disc_name != expected_dirname {
                    let err_msg = format!("Missing subdir {}", expected_dirname);
                    eprintln!("{}", err_msg);
                    return false;
                }
            }
        }
        true
    }

    match entry {
        Entry::SingleDiscAlbum(_ad, _dd) => {
            rclogger.log(&RipCheckLoggerMessage::Info(
                "check_entry::check_entry_disc_numbering",
                "skipping SingleDiscAlbum",
            ))?;
            Ok(true)
        }
        Entry::ManyDiscAlbum(ad, ddv) => Ok(check_disc_numbering(ad, ddv, 1)),
        Entry::ManyDiscAlbumWithRoot(ad, ddv) => Ok(check_disc_numbering(ad, ddv, 0)),
        Entry::NoneDiscAlbum(_ad) => {
            rclogger.log(&RipCheckLoggerMessage::Info(
                "check_entry::check_entry_disc_numbering",
                "skipping NoneDiscAlbum",
            ))?;
            Ok(true)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::check_entry_disc_numbering;
    use crate::music_db::{album_data::AlbumData, disc_data::DiscData, entry::Entry};
    use crate::ripcheck_logger::RipCheckLogger;
    use std::path::PathBuf;

    #[test]
    fn test_check_disc_numbering() {
        //
        // Test type:
        //      Positive test: OneDiscAlbum
        // Test description:
        //      CD1
        //

        let mut rclogger = RipCheckLogger::builder().color_tty(true).build();

        let ad = AlbumData::builder(&PathBuf::from(
            "flac/Insomnium - Songs of the Dusk (Anno 1696 Bonus Disc) - 2023",
        ))
        .artist("Insomnium")
        .album("Songs of the Dusk (Anno 1696 Bonus Disc)")
        .year(2023)
        .build();
        let disc = DiscData::builder(&PathBuf::from(
            "flac/Insomnium - Songs of the Dusk (Anno 1696 Bonus Disc) - 2023",
        ))
        .build();
        let entry = Entry::SingleDiscAlbum(ad, disc);
        assert!(check_entry_disc_numbering(&entry, &mut rclogger).unwrap());

        //
        // Test type:
        //      Positive test: ManyDiscAlbum
        // Test description:
        //      CD1, CD2
        //
        let ad = AlbumData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023"))
            .artist("testartist0")
            .album("testalbum0")
            .year(2023)
            .build();
        let disc1 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD1"))
            .disc_name("CD1")
            .disc_nro(1)
            .build();
        let disc2 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD2"))
            .disc_name("CD2")
            .disc_nro(2)
            .build();
        let entry = Entry::ManyDiscAlbum(ad, vec![disc1, disc2]);
        assert!(check_entry_disc_numbering(&entry, &mut rclogger).unwrap());

        //
        // Test type:
        //      Negative test: ManyDiscAlbum
        // Test description:
        //      CD1, CD3
        //
        let ad = AlbumData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023"))
            .artist("testartist0")
            .album("testalbum0")
            .year(2023)
            .build();
        let disc1 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD1"))
            .disc_name("CD1")
            .disc_nro(1)
            .build();
        let disc3 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD3"))
            .disc_name("CD3")
            .disc_nro(3)
            .build();
        let entry = Entry::ManyDiscAlbum(ad, vec![disc1, disc3]);
        assert!(!check_entry_disc_numbering(&entry, &mut rclogger).unwrap());

        //
        // Test type:
        //      Negative test: ManyDiscAlbum
        // Test description:
        //      CD1, CD2, CD4
        //
        let ad = AlbumData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023"))
            .artist("testartist0")
            .album("testalbum0")
            .year(2023)
            .build();
        let disc1 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD1"))
            .disc_name("CD1")
            .disc_nro(1)
            .build();
        let disc2 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD2"))
            .disc_name("CD2")
            .disc_nro(1)
            .build();
        let disc4 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD4"))
            .disc_name("CD4")
            .disc_nro(1)
            .build();
        let entry = Entry::ManyDiscAlbum(ad, vec![disc1, disc2, disc4]);
        assert!(!check_entry_disc_numbering(&entry, &mut rclogger).unwrap());

        //
        // Test type:
        //      Positive test: ManyDiscAlbumWithRoot
        // Test description:
        //      <root>, CD1, CD2
        //
        let ad = AlbumData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023"))
            .artist("testartist0")
            .album("testalbum0")
            .year(2023)
            .build();
        let disc_root =
            DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023")).build();
        let disc1 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD1"))
            .disc_name("CD1")
            .disc_nro(1)
            .build();
        let disc2 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD2"))
            .disc_name("CD2")
            .disc_nro(2)
            .build();
        let entry = Entry::ManyDiscAlbumWithRoot(ad, vec![disc_root, disc1, disc2]);
        assert!(check_entry_disc_numbering(&entry, &mut rclogger).unwrap());

        //
        // Test type:
        //      Negative test: ManyDiscAlbumWithRoot
        // Test description:
        //      <root>, CD1, CD2, CD4
        //
        let ad = AlbumData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023"))
            .artist("testartist0")
            .album("testalbum0")
            .year(2023)
            .build();
        let disc_root = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023"))
            .disc_nro(0)
            .build();
        let disc1 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD1"))
            .disc_name("CD1")
            .disc_nro(1)
            .build();
        let disc2 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD2"))
            .disc_name("CD2")
            .disc_nro(2)
            .build();
        let disc4 = DiscData::builder(&PathBuf::from("flac/testartist0 - testalbum 0 - 2023/CD4"))
            .disc_name("CD4")
            .disc_nro(4)
            .build();
        let entry = Entry::ManyDiscAlbumWithRoot(ad, vec![disc_root, disc1, disc2, disc4]);
        assert!(!check_entry_disc_numbering(&entry, &mut rclogger).unwrap());
    }
}
