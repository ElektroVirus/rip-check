use crate::{
    music_db::{album_data::AlbumData, disc_data::DiscData, entry::Entry, trackdata::TrackData},
    music_db_check::{cueripper_metadata::AlbumDate, cueripper_metadata::CueripperMetadata},
    ripcheck_logger::{RipCheckLogger, RipCheckLoggerMessage},
    util::replace_characters,
};
use chrono::Datelike;
use flac::metadata;
use std::io;

pub fn check_metadata(entry: &Entry, rclogger: &mut RipCheckLogger) -> Result<bool, io::Error> {
    match entry {
        Entry::SingleDiscAlbum(ad, dd) => check_discdata(entry, ad, dd, rclogger)?,
        Entry::ManyDiscAlbum(ad, ddv) => check_discdatas(entry, ad, ddv, rclogger)?,
        Entry::ManyDiscAlbumWithRoot(ad, ddv) => check_discdatas(entry, ad, ddv, rclogger)?,
        Entry::NoneDiscAlbum(ad) => {
            rclogger.log(&RipCheckLoggerMessage::Info(
                "check_metadata::check_metadata",
                format!("skipping NoneDiscAlbum {}", ad),
            ))?;
        }
    };
    Ok(true)
}

fn check_discdatas(
    entry: &Entry,
    ad: &AlbumData,
    ddv: &Vec<DiscData>,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    for dd in ddv {
        check_discdata(entry, ad, dd, rclogger)?;
    }
    Ok(())
}

fn check_discdata(
    entry: &Entry,
    ad: &AlbumData,
    dd: &DiscData,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    for track in dd.tracks() {
        check_track(entry, ad, dd, track, rclogger)?;
    }
    Ok(())
}

fn check_track(
    entry: &Entry,
    ad: &AlbumData,
    dd: &DiscData,
    track: &TrackData,
    rclogger: &mut RipCheckLogger,
) -> Result<bool, io::Error> {
    let fpath = track.path().as_os_str().to_str().unwrap();
    let cm: CueripperMetadata = match metadata::get_vorbis_comment(fpath) {
        Ok(vorbis_comment) => CueripperMetadata::from(
            track.path(),
            &vorbis_comment.vendor_string,
            vorbis_comment.comments,
        ),
        Err(error) => {
            println!("{:?}", error);
            return Ok(false);
        }
    };

    if let Err(msg) = check_track_disc_numbers(&cm, dd) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_track_disc_numbers",
            msg,
        ))?;
    }
    if let Err(msg) = check_track_artist_by_albumdata(&cm, ad) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_track_artist_by_albumdata",
            msg,
        ))?;
    }
    if let Err(msg) = check_track_artist_by_trackdata(&cm, track) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_track_artist_by_trackdata",
            msg,
        ))?;
    }
    if let Err(msg) = check_track_title(&cm, track) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed("check_track_title", msg))?;
    }
    if let Err(msg) = check_track_album(&cm, ad) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed("check_track_album", msg))?;
    }
    if let Err(msg) = check_track_number(&cm, track) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_track_number",
            msg,
        ))?;
    }
    if let Err(msg) = check_track_number_in_limits(&cm, track, dd) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_track_number_in_limits",
            msg,
        ))?;
    }
    if let Err(msg) = check_tracktotal(&cm, dd) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed("check_tracktotal", msg))?;
    }
    if let Err(msg) = check_disctotal(&cm, entry) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed("check_disctotal", msg))?;
    }
    if let Err(msg) = check_discnumber(&cm, dd) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed("check_discnumber", msg))?;
    }
    if let Err(msg) = check_date(&cm, ad) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed("check_date", msg))?;
    }
    if let Err(msg) = check_release_date(&cm, ad) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_release_date",
            msg,
        ))?;
    }
    if let Err(msg) = check_ctdbtrackconfidence(&cm) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_ctdbtrackconfidence",
            msg,
        ))?;
    }
    if let Err(msg) = check_ctdbdiscconfidence(&cm) {
        rclogger.log(&RipCheckLoggerMessage::TestFailed(
            "check_ctdbdiscconfidence",
            msg,
        ))?;
    }

    Ok(true)
}

fn check_track_disc_numbers(cm: &CueripperMetadata, dd: &DiscData) -> Result<(), String> {
    if let Some(disknro) = cm.discnumber {
        if disknro != dd.disc_nro() {
            return Err(format!(
                "{}: Track disk number in metadata and directory do not match: {} != {}",
                cm.fs_path.display(),
                disknro,
                dd.disc_nro()
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid disc numbering found in metadata",
            cm.fs_path.display()
        ));
    }
    Ok(())
}

fn check_track_artist_by_albumdata(cm: &CueripperMetadata, ad: &AlbumData) -> Result<(), String> {
    if let Some(mdata_artist) = &cm.artist {
        if let Ok(ad_artist) = ad.artist() {
            if mdata_artist.as_str() != ad_artist.as_str() {
                return Err(format!(
                    "{}: Artist in metadata and album directory do not match: {} != {}",
                    cm.fs_path.display(),
                    mdata_artist,
                    ad_artist
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid artist found in album dirname",
                cm.fs_path.display()
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid artist found in track metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_track_artist_by_trackdata(
    cm: &CueripperMetadata,
    track: &TrackData,
) -> Result<(), String> {
    if let Some(mdata_artist) = &cm.artist {
        if let Ok(td_artist) = track.artist() {
            if mdata_artist.as_str() != td_artist.as_str() {
                return Err(format!(
                    "{}: Artist in track metadata and directory do not match: {} != {}",
                    cm.fs_path.display(),
                    mdata_artist,
                    td_artist
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid artist found in track filename",
                cm.fs_path.display(),
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid artist found in track metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_track_title(cm: &CueripperMetadata, track: &TrackData) -> Result<(), String> {
    if let Some(mdata_title) = &cm.title {
        if let Ok(file_title) = track.title() {
            let mdata_title_replaced = replace_characters(mdata_title);
            if mdata_title_replaced != file_title {
                return Err(format!(
                    "{}: Track title in metadata and filename do not match: {} != {}",
                    cm.fs_path.display(),
                    mdata_title_replaced,
                    file_title
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid title found in filename",
                cm.fs_path.display(),
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid title name found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_track_album(cm: &CueripperMetadata, ad: &AlbumData) -> Result<(), String> {
    if let Some(mdata_album) = &cm.album {
        if let Ok(file_album) = ad.album() {
            let mdata_album_replaced = replace_characters(mdata_album);
            if mdata_album_replaced != file_album {
                return Err(format!(
                    "{}: Album name in metadata and directory do not match: {} != {}",
                    cm.fs_path.display(),
                    mdata_album_replaced,
                    file_album
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid album name found in directory",
                cm.fs_path.display(),
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid album name found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_track_number(cm: &CueripperMetadata, track: &TrackData) -> Result<(), String> {
    if let Some(mdata_track_nro) = cm.tracknumber {
        if let Ok(file_track_nro) = track.number() {
            if mdata_track_nro != file_track_nro as u32 {
                return Err(format!(
                    "Track nro in metadata and filename do not match: {} != {}",
                    mdata_track_nro, file_track_nro
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid track number found in filename",
                cm.fs_path.display(),
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid track number found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_track_number_in_limits(
    cm: &CueripperMetadata,
    track: &TrackData,
    dd: &DiscData,
) -> Result<(), String> {
    let nro_of_tracks_on_disk = dd.nro_of_tracks();

    if let Some(mdata_track_nro) = cm.tracknumber {
        if mdata_track_nro > (nro_of_tracks_on_disk).try_into().unwrap() {
            return Err(format!(
                "{}: Track nro in metadata is bigger than number of tracks on disk: {} > {}",
                cm.fs_path.display(),
                mdata_track_nro,
                nro_of_tracks_on_disk
            ));
        } else if mdata_track_nro < 1 {
            return Err(format!(
                "{}: Track nro in metadata is smaller than 1: {}",
                cm.fs_path.display(),
                mdata_track_nro
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid track number found in metadata",
            cm.fs_path.display(),
        ));
    }

    if let Ok(file_track_nro) = track.number() {
        if file_track_nro > (nro_of_tracks_on_disk).try_into().unwrap() {
            return Err(format!(
                "{}: Track nro in filename is bigger than number of tracks on disk: {} > {}",
                cm.fs_path.display(),
                file_track_nro,
                nro_of_tracks_on_disk
            ));
        } else if file_track_nro < 1 {
            return Err(format!(
                "{}: Track nro in filename is smaller than 1: {}",
                cm.fs_path.display(),
                file_track_nro
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid track number found in filename",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

/// Checks that the number in metadata comment field TRACKTOTAL is equal to
/// audio files in cd-directory
fn check_tracktotal(cm: &CueripperMetadata, dd: &DiscData) -> Result<(), String> {
    if let Some(mdata_tracktotal) = cm.tracktotal {
        let file_tracktotal = dd.tracks().len();
        if mdata_tracktotal as usize != file_tracktotal {
            return Err(format!(
                "{}: Tracktotal in metadata does not match with number of audiofiles in disc directory: {} != {}",
                cm.fs_path.display(),
                mdata_tracktotal, file_tracktotal
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid tracktotal found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_disctotal(cm: &CueripperMetadata, entry: &Entry) -> Result<(), String> {
    if let Some(mdata_disctotal) = cm.disctotal {
        let file_disctotal = entry.nro_of_discs();
        if mdata_disctotal as usize != file_disctotal {
            return Err(format!(
                "{}: Disctotal in metadata does not match with number of disc directories: {} != {}",
                cm.fs_path.display(),
                mdata_disctotal,
                file_disctotal
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid disctotal found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_discnumber(cm: &CueripperMetadata, dd: &DiscData) -> Result<(), String> {
    if let Some(mdata_discnumber) = cm.discnumber {
        let file_discnumber = dd.disc_nro();
        if mdata_discnumber != file_discnumber {
            return Err(format!(
                "{}: Discnumber in metadata does not match with number of disc directory: {} != {}",
                cm.fs_path.display(),
                mdata_discnumber,
                file_discnumber
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid discnumber found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

/// Compare metadata DATE-field's year attribute to album directory year
fn check_date(cm: &CueripperMetadata, ad: &AlbumData) -> Result<(), String> {
    if let Some(mdata_date) = &cm.date {
        let mdata_year = match mdata_date {
            AlbumDate::Date(d) => d.year(),
            AlbumDate::Year(y) => *y,
            AlbumDate::Unknown(s) => {
                return Err(format!(
                    "{}: DATE found in metadata contains illegal string: {}",
                    cm.fs_path.display(),
                    s
                ));
            }
        };
        if let Ok(albumdir_year) = ad.year() {
            if mdata_year != albumdir_year as i32 {
                return Err(format!(
                    "{}: Year in metadata's DATE and year in album directory name do not match: {} != {}",
                    cm.fs_path.display(),
                    mdata_year, albumdir_year
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid year found in album directory name",
                cm.fs_path.display(),
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid field DATE found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

/// Compare metadata DATE-field's year attribute to album directory year
fn check_release_date(cm: &CueripperMetadata, ad: &AlbumData) -> Result<(), String> {
    // TODO: This checks only the year
    if let Some(mdata_date) = &cm.release_date {
        let mdata_year = match mdata_date {
            AlbumDate::Date(d) => d.year(),
            AlbumDate::Year(y) => *y,
            AlbumDate::Unknown(s) => {
                return Err(format!(
                    "{}: DATE found in metadata contains illegal string: {}",
                    cm.fs_path.display(),
                    s
                ));
            }
        };
        if let Ok(albumdir_year) = ad.year() {
            if mdata_year != albumdir_year as i32 {
                return Err(format!(
                    "{}: Year in metadata's DATE and year in album directory name do not match: {} != {}",
                    cm.fs_path.display(),
                    mdata_year, albumdir_year
                ));
            }
        } else {
            return Err(format!(
                "{}: No valid year found in album directory name",
                cm.fs_path.display(),
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid field RELEASE DATE found in metadata",
            cm.fs_path.display(),
        ));
    }
    Ok(())
}

fn check_ctdbtrackconfidence(cm: &CueripperMetadata) -> Result<(), String> {
    if let Some(confidence) = cm.ctdbtrackconfidence {
        if confidence.0 != confidence.1 {
            return Err(format!(
                "{}: CTDBTRACKCONFIDENCE is not full: {}/{}",
                cm.fs_path.display(),
                confidence.0,
                confidence.1
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid CTDBTRACKCONFIDENCE found in metadata",
            cm.fs_path.display()
        ));
    }
    Ok(())
}

fn check_ctdbdiscconfidence(cm: &CueripperMetadata) -> Result<(), String> {
    if let Some(confidence) = cm.ctdbdiscconfidence {
        if confidence.0 != confidence.1 {
            return Err(format!(
                "{}: CTDBDISCCONFIDENCE is not full: {}/{}",
                cm.fs_path.display(),
                confidence.0,
                confidence.1
            ));
        }
    } else {
        return Err(format!(
            "{}: No valid CTDBDISCCONFIDENCE found in metadata",
            cm.fs_path.display()
        ));
    }

    Ok(())
}

// Unchecked metadata fields:
//
// CDTOC
//
// // Should these be checked also
// RELEASECOUNTRY
// PUBLISHER
// LABELNO

#[cfg(test)]
mod tests {
    use crate::music_db::album_data::AlbumData;
    use crate::music_db::disc_data::DiscData;
    use crate::music_db_check::check_metadata::*;
    use crate::music_db_check::cueripper_metadata::CueripperMetadata;
    use chrono::NaiveDate;
    use std::path::PathBuf;

    #[test]
    fn test_check_track_disc_numbers() {
        // Positive: both CueripperMetadata and DiscData have CD number as 1
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .discnumber(1)
            .disctotal(1)
            .build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).disc_nro(1).build();
        assert!(check_track_disc_numbers(&crmd, &dd).is_ok());

        // Negative: CueripperMetadata has CD number 2, DiscData cd number 1
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .discnumber(2)
            .disctotal(2)
            .build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).disc_nro(1).build();
        assert!(check_track_disc_numbers(&crmd, &dd).is_err());

        // Negative: CueripperMetadata has no CD number in metadata
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).disc_nro(1).build();
        assert!(check_track_disc_numbers(&crmd, &dd).is_err());
    }

    #[test]
    fn test_check_track_artist_by_albumdata() {
        // Positive: Metadata artist matches AlbumData artist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .artist("Testartist")
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/"))
            .artist("Testartist")
            .build();
        assert!(check_track_artist_by_albumdata(&crmd, &ad).is_ok());

        // Negative: Metadata artist does not match AlbumData artist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .artist("Testartist0")
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/"))
            .artist("Testartist1")
            .build();
        assert!(check_track_artist_by_albumdata(&crmd, &ad).is_err());

        // Negative: Artist does not exist in metadata
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/"))
            .artist("Testartist1")
            .build();
        assert!(check_track_artist_by_albumdata(&crmd, &ad).is_err());

        // Negative: Artist does not exist in AlbumData
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .artist("Testartist0")
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).build();
        assert!(check_track_artist_by_albumdata(&crmd, &ad).is_err());
    }

    #[test]
    fn test_check_track_artist_by_trackdata() {
        // Positive: Metadata artist matches TrackData artist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .artist("Testartist")
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/"))
            .artist("Testartist")
            .build();
        assert!(check_track_artist_by_trackdata(&crmd, &track).is_ok());

        // Negative: Metadata artist does not match TrackData artist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .artist("Testartist0")
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/"))
            .artist("Testartist1")
            .build();
        assert!(check_track_artist_by_trackdata(&crmd, &track).is_err());

        // Negative: Album artist does not exist in metadata
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/"))
            .artist("Testartist")
            .build();
        assert!(check_track_artist_by_trackdata(&crmd, &track).is_err());

        // Negative: Album artist does not exist in TrackData
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .artist("Testartist")
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/")).build();
        assert!(check_track_artist_by_trackdata(&crmd, &track).is_err());
    }

    #[test]
    fn test_check_track_title() {
        // Positive: Metadata title matches TrackData title
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .title("Testtitle")
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/"))
            .title("Testtitle")
            .build();
        assert!(check_track_title(&crmd, &track).is_ok());

        // Negative: Metadata title does not match TrackData title
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .title("Testtitle0")
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/"))
            .title("Testtitle1")
            .build();
        assert!(check_track_title(&crmd, &track).is_err());

        // Negative: Metadata title does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/"))
            .title("Testtitle")
            .build();
        assert!(check_track_title(&crmd, &track).is_err());

        // Negative: TrackData title does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .title("Testtitle")
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/")).build();
        assert!(check_track_title(&crmd, &track).is_err());
    }

    #[test]
    fn test_check_track_album() {
        // Positive: Metadata album matches AlbumData album
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .album("Testalbum")
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/"))
            .album("Testalbum")
            .build();
        assert!(check_track_album(&crmd, &ad).is_ok());

        // Negative: Metadata album does not match AlbumData album
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .album("Testalbum0")
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/"))
            .album("Testalbum1")
            .build();
        assert!(check_track_album(&crmd, &ad).is_err());

        // Negative: Album does not exist in metadata
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/"))
            .album("Testalbum")
            .build();
        assert!(check_track_album(&crmd, &ad).is_err());

        // Negative: Album does not exist in AlbumData
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .album("Testalbum")
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).build();
        assert!(check_track_album(&crmd, &ad).is_err());
    }

    #[test]
    fn test_check_track_number() {
        // Positive: Metadata track number matches TrackData track number
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .tracknumber(7)
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/")).number(7).build();
        assert!(check_track_number(&crmd, &track).is_ok());

        // Negative: Metadata track number does not match TrackData track number
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .tracknumber(7)
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/")).number(1).build();
        assert!(check_track_number(&crmd, &track).is_err());

        // Negative: Metadata track number does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/")).number(7).build();
        assert!(check_track_number(&crmd, &track).is_err());

        // Negative: TrackData track number does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .tracknumber(7)
            .build();
        let track: TrackData = TrackData::builder(&PathBuf::from("/")).build();
        assert!(check_track_number(&crmd, &track).is_err());
    }

    #[test]
    fn test_check_tracktotal() {
        // Positive: Tracks equal
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .tracktotal(6)
            .build();
        let mut tracks = Vec::<TrackData>::new();
        for i in 0..6 {
            let track = TrackData::builder(&PathBuf::from("/")).number(i).build();
            tracks.push(track);
        }
        let dd: DiscData = DiscData::builder(&PathBuf::from("/"))
            .tracks(tracks)
            .build();
        assert!(check_tracktotal(&crmd, &dd).is_ok());

        // Negative: Tracks do not equal
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .tracktotal(7)
            .build();
        let mut tracks = Vec::<TrackData>::new();
        for i in 0..3 {
            let track = TrackData::builder(&PathBuf::from("/")).number(i).build();
            tracks.push(track);
        }
        let dd: DiscData = DiscData::builder(&PathBuf::from("/"))
            .tracks(tracks)
            .build();
        assert!(check_tracktotal(&crmd, &dd).is_err());

        // Negative: No tracktotal in metadata
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).build();
        assert!(check_tracktotal(&crmd, &dd).is_err());
    }

    #[test]
    fn test_check_disctotal() {
        //
        // Entry::SingleDiscAlbum
        //

        // Positive: One disc
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(1)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::SingleDiscAlbum(ad, dd);
        assert!(check_disctotal(&crmd, &entry).is_ok());

        // Negative: 2 discs
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(2)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::SingleDiscAlbum(ad, dd);
        assert!(check_disctotal(&crmd, &entry).is_err());

        // Negative: 0 discs
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(0)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::SingleDiscAlbum(ad, dd);
        assert!(check_disctotal(&crmd, &entry).is_err());

        //
        // Entry::ManyDiscAlbum
        //

        // Positive: Two discs for Entry::ManyDiscAlbum with 2 discs
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(2)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd1 = DiscData::builder(&PathBuf::from("/")).build();
        let dd2 = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::ManyDiscAlbum(ad, vec![dd1, dd2]);
        assert!(check_disctotal(&crmd, &entry).is_ok());

        // Negative: Two discs for Entry::ManyDiscAlbum with 1 disc
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(2)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd1 = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::ManyDiscAlbum(ad, vec![dd1]);
        assert!(check_disctotal(&crmd, &entry).is_err());

        //
        // Entry::ManyDiscAlbumWithRoot
        //

        // Positive: Two discs for Entry::ManyDiscAlbumWithRoot with 2 discs
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(2)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd1 = DiscData::builder(&PathBuf::from("/")).build();
        let dd2 = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::ManyDiscAlbumWithRoot(ad, vec![dd1, dd2]);
        assert!(check_disctotal(&crmd, &entry).is_ok());

        // Negative: Two discs for Entry::ManyDiscAlbum with 1 disc
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .disctotal(2)
            .build();
        let ad = AlbumData::builder(&PathBuf::from("/")).build();
        let dd1 = DiscData::builder(&PathBuf::from("/")).build();
        let entry = Entry::ManyDiscAlbumWithRoot(ad, vec![dd1]);
        assert!(check_disctotal(&crmd, &entry).is_err());

        //
        // Entry::NoneDiscAlbum
        //

        // Won't test, this is not possible!
    }

    #[test]
    fn test_check_discnumber() {
        // Positive: Metadata disc number matches DiscData disc number
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .discnumber(2)
            .build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).disc_nro(2).build();
        assert!(check_discnumber(&crmd, &dd).is_ok());

        // Negative: Metadata disc number does not match DiscData disc number
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .discnumber(2)
            .build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).disc_nro(3).build();
        assert!(check_discnumber(&crmd, &dd).is_err());

        // Negative: Metadata disc number does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).disc_nro(2).build();
        assert!(check_discnumber(&crmd, &dd).is_err());

        // Negative: DiscData disc number does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .discnumber(2)
            .build();
        let dd: DiscData = DiscData::builder(&PathBuf::from("/")).build();
        assert!(check_discnumber(&crmd, &dd).is_err());
    }

    #[test]
    fn test_check_date() {
        // Positive: Metadata date matches AlbumData date using AlbumDate::Year
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .date(AlbumDate::Year(2023))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2023).build();
        assert!(check_date(&crmd, &ad).is_ok());

        // Positive: Metadata date matches AlbumData date using AlbumDate::Date
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .date(AlbumDate::Date(
                NaiveDate::from_ymd_opt(2023, 7, 24).unwrap(),
            ))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2023).build();
        assert!(check_date(&crmd, &ad).is_ok());

        // Negative: Metadata date is of type AlbumDate::Unknown
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .date(AlbumDate::Unknown("Brööt".to_owned()))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2019).build();
        assert!(check_date(&crmd, &ad).is_err());

        // Negative: Metadata date does not match AlbumData date using AlbumDate::Year
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .date(AlbumDate::Year(2023))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2019).build();
        assert!(check_date(&crmd, &ad).is_err());

        // Negative: Metadata date does not match AlbumData date using AlbumDate::Date
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .date(AlbumDate::Date(
                NaiveDate::from_ymd_opt(2023, 7, 24).unwrap(),
            ))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2019).build();
        assert!(check_date(&crmd, &ad).is_err());

        // Negative: Metadata date does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2023).build();
        assert!(check_date(&crmd, &ad).is_err());

        // Negative: AlbumData date does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .date(AlbumDate::Year(2023))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).build();
        assert!(check_date(&crmd, &ad).is_err());
    }

    #[test]
    fn test_check_release_date() {
        // Positive: Metadata date matches AlbumData date using AlbumDate::Year
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .release_date(AlbumDate::Year(2023))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2023).build();
        assert!(check_release_date(&crmd, &ad).is_ok());

        // Positive: Metadata date matches AlbumData date using AlbumDate::Date
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .release_date(AlbumDate::Date(
                NaiveDate::from_ymd_opt(2023, 7, 24).unwrap(),
            ))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2023).build();
        assert!(check_release_date(&crmd, &ad).is_ok());

        // Negative: Metadata date is of type AlbumDate::Unknown
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .release_date(AlbumDate::Unknown("Brööt".to_owned()))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2019).build();
        assert!(check_release_date(&crmd, &ad).is_err());

        // Negative: Metadata date does not match AlbumData date using AlbumDate::Year
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .release_date(AlbumDate::Year(2023))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2019).build();
        assert!(check_release_date(&crmd, &ad).is_err());

        // Negative: Metadata date does not match AlbumData date using AlbumDate::Date
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .release_date(AlbumDate::Date(
                NaiveDate::from_ymd_opt(2023, 7, 24).unwrap(),
            ))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2019).build();
        assert!(check_release_date(&crmd, &ad).is_err());

        // Negative: Metadata date does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).year(2023).build();
        assert!(check_release_date(&crmd, &ad).is_err());

        // Negative: AlbumData date does not exist
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .release_date(AlbumDate::Year(2023))
            .build();
        let ad: AlbumData = AlbumData::builder(&PathBuf::from("/")).build();
        assert!(check_release_date(&crmd, &ad).is_err());
    }

    #[test]
    fn test_check_ctdbtrackconfidence() {
        // Positive: Confidence is full
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .ctdbtrackconfidence((1337, 1337))
            .build();
        assert!(check_ctdbtrackconfidence(&crmd).is_ok());

        // Negative: Confidence is not full
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .ctdbtrackconfidence((1337, 0))
            .build();
        assert!(check_ctdbtrackconfidence(&crmd).is_err());

        // Negative: Confidence is None
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        assert!(check_ctdbtrackconfidence(&crmd).is_err());
    }

    #[test]
    fn test_check_ctdbdiscconfidence() {
        // Positive: Confidence is full
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .ctdbdiscconfidence((1337, 1337))
            .build();
        assert!(check_ctdbdiscconfidence(&crmd).is_ok());

        // Negative: Confidence is not full
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/"))
            .ctdbdiscconfidence((1337, 0))
            .build();
        assert!(check_ctdbdiscconfidence(&crmd).is_err());

        // Negative: Confidence is None
        let crmd: CueripperMetadata = CueripperMetadata::builder(&PathBuf::from("/")).build();
        assert!(check_ctdbdiscconfidence(&crmd).is_err());
    }
}
