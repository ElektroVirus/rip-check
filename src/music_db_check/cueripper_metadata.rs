#![cfg_attr(rustfmt, rustfmt_skip)]
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use chrono::naive::NaiveDate;
use crate::util;

#[derive(Debug, Clone)]
pub enum AlbumDate {
    Date(NaiveDate),
    Year(i32),
    Unknown(String),
}

#[derive(Debug)]
pub struct CueripperMetadata {
    // This is only used for identifying and debug printing
    pub fs_path:                PathBuf,
    pub vendor_string:          Option<String>,
    pub discnumber:             Option<u32>,
    pub artist:                 Option<String>,
    pub tracktotal:             Option<u32>,
    pub tracknumber:            Option<u32>,
    pub title:                  Option<String>,
    pub cdtoc:                  Option<String>,
    pub album:                  Option<String>,
    pub date:                   Option<AlbumDate>,
    pub releasecountry:         Option<String>,
    pub ctdbtrackconfidence:    Option<(i32, i32)>,
    pub ctdbdiscconfidence:     Option<(i32, i32)>,
    pub comment:                Option<String>,
    pub labelno:                Option<String>,
    pub disctotal:              Option<u32>,
    pub publisher:              Option<String>,
    pub release_date:           Option<AlbumDate>,
    pub genre:                  Option<String>,
    pub discsubtitle:           Option<String>,
}

impl CueripperMetadata {
    #[allow(dead_code)]
    pub fn builder(path: &Path) -> CueripperMetadataBuilder {
        CueripperMetadataBuilder::new(path)
    }

    pub fn new(path: &Path) -> Self {
        Self {
            fs_path:                path.to_path_buf(),
            vendor_string:          None,
            discnumber:             None,
            artist:                 None,
            tracktotal:             None,
            tracknumber:            None,
            title:                  None,
            cdtoc:                  None,
            album:                  None,
            date:                   None,
            releasecountry:         None,
            ctdbtrackconfidence:    None,
            ctdbdiscconfidence:     None,
            comment:                None,
            labelno:                None,
            disctotal:              None,
            publisher:              None,
            release_date:           None,
            genre:                  None,
            discsubtitle:           None,
        }
    }

    pub fn from(path: &Path, vendor_string: &str, comments: HashMap<String, String>)
        -> Self
        {
        let mut cm = Self::new(path);
        cm.vendor_string = Some(vendor_string.to_string());

        for key in comments.keys() {
            match key.as_str() {
                "DISCNUMBER"            => cm.discnumber            = Some(comments[key].parse().expect("")),
                "ARTIST"                => cm.artist                = Some(comments[key].to_string()),
                "TRACKTOTAL"            => cm.tracktotal            = Some(comments[key].parse().expect("")),
                "TRACKNUMBER"           => cm.tracknumber           = Some(comments[key].parse().expect("")),
                "TITLE"                 => cm.title                 = Some(comments[key].to_string()),
                "CDTOC"                 => cm.cdtoc                 = Some(comments[key].to_string()),
                "ALBUM"                 => cm.album                 = Some(comments[key].to_string()),
                "DATE"                  => cm.date                  = Some(Self::parse_date(&comments[key])),
                "RELEASECOUNTRY"        => cm.releasecountry        = Some(comments[key].to_string()),
                "CTDBTRACKCONFIDENCE"   => cm.ctdbtrackconfidence   = Self::parse_confidence(&comments[key]),
                "CTDBDISCCONFIDENCE"    => cm.ctdbdiscconfidence    = Self::parse_confidence(&comments[key]),
                "COMMENT"               => cm.comment               = Some(comments[key].to_string()),
                "LABELNO"               => cm.labelno               = Some(comments[key].to_string()),
                "DISCTOTAL"             => cm.disctotal             = Some(comments[key].parse().expect("")),
                "PUBLISHER"             => cm.publisher             = Some(comments[key].to_string()),
                "RELEASE DATE"          => cm.release_date          = Some(Self::parse_date(&comments[key])),
                "GENRE"                 => cm.genre                 = Some(comments[key].to_string()),
                "DISCSUBTITLE"          => cm.discsubtitle          = Some(comments[key].to_string()),
                _                       => {println!("cueripper_metadata: Unknown tag: {}={}", key, comments[key])}
            }
        }
        cm
    }

    /// Try parsing different date strings.
    /// Some albums may have only year specified in their date-field, whereas
    /// others may have the whole date.
    ///
    /// Returns AlbumDate struct
    ///
    fn parse_date(datestr: &str) -> AlbumDate {
        if let Ok(res) = NaiveDate::parse_from_str(datestr, "%Y-%m-%d") {
            return AlbumDate::Date(res);
        }

        if let Ok(res) = datestr.parse::<i32>() {
            return AlbumDate::Year(res);
        }
        AlbumDate::Unknown(datestr.to_owned())
    }

    fn parse_confidence(confidence: &str) -> Option<(i32, i32)> {
        let confidencevec = confidence.split(
            util::METADATA_CONFIDENCE_DELIMETER).collect::<Vec<&str>>();
        if confidencevec.len() < 2 {
            return None;
        }

        let first = confidencevec[0].parse::<i32>().ok();
        let second = confidencevec[1].parse::<i32>().ok();

        match (first, second) {
            (Some(first), Some(second)) => Some((first, second)),
            _ => None,
        }
    }
}

pub struct CueripperMetadataBuilder {
    // This is only used for identifying and debug printing
    fs_path:                PathBuf,
    vendor_string:          Option<String>,
    discnumber:             Option<u32>,
    artist:                 Option<String>,
    tracktotal:             Option<u32>,
    tracknumber:            Option<u32>,
    title:                  Option<String>,
    cdtoc:                  Option<String>,
    album:                  Option<String>,
    date:                   Option<AlbumDate>,
    releasecountry:         Option<String>,
    ctdbtrackconfidence:    Option<(i32, i32)>,
    ctdbdiscconfidence:     Option<(i32, i32)>,
    comment:                Option<String>,
    labelno:                Option<String>,
    disctotal:              Option<u32>,
    publisher:              Option<String>,
    release_date:           Option<AlbumDate>,
    genre:                  Option<String>,
    discsubtitle:           Option<String>,
}

impl CueripperMetadataBuilder {
    #[allow(dead_code)]
    pub fn new(path: &Path) -> Self {
        CueripperMetadataBuilder {
            fs_path: path.to_path_buf(),
            vendor_string:          None,
            discnumber:             None,
            artist:                 None,
            tracktotal:             None,
            tracknumber:            None,
            title:                  None,
            cdtoc:                  None,
            album:                  None,
            date:                   None,
            releasecountry:         None,
            ctdbtrackconfidence:    None,
            ctdbdiscconfidence:     None,
            comment:                None,
            labelno:                None,
            disctotal:              None,
            publisher:              None,
            release_date:           None,
            genre:                  None,
            discsubtitle:           None,
        }
    }

    #[allow(dead_code)]
    pub fn vendor_string(&mut self, vendor_string: &str) -> &mut Self {
        self.vendor_string = Some(vendor_string.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn discnumber(&mut self, discnumber: u32) -> &mut Self {
        self.discnumber = Some(discnumber);
        self
    }

    #[allow(dead_code)]
    pub fn artist(&mut self, artist: &str) -> &mut Self {
        self.artist = Some(artist.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn tracktotal(&mut self, tracktotal: u32) -> &mut Self {
        self.tracktotal = Some(tracktotal);
        self
    }

    #[allow(dead_code)]
    pub fn tracknumber(&mut self, tracknumber: u32) -> &mut Self {
        self.tracknumber = Some(tracknumber);
        self
    }

    #[allow(dead_code)]
    pub fn title(&mut self, title: &str) -> &mut Self {
        self.title = Some(title.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn cdtoc(&mut self, cdtoc: &str) -> &mut Self {
        self.cdtoc = Some(cdtoc.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn album(&mut self, album: &str) -> &mut Self {
        self.album = Some(album.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn date(&mut self, date: AlbumDate) -> &mut Self {
        self.date = Some(date);
        self
    }

    #[allow(dead_code)]
    pub fn releasecountry(&mut self, releasecountry: &str) -> &mut Self {
        self.releasecountry = Some(releasecountry.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn ctdbtrackconfidence(&mut self, ctdbtrackconfidence: (i32, i32)) -> &mut Self {
        self.ctdbtrackconfidence = Some(ctdbtrackconfidence);
        self
    }

    #[allow(dead_code)]
    pub fn ctdbdiscconfidence(&mut self, ctdbdiscconfidence: (i32, i32)) -> &mut Self {
        self.ctdbdiscconfidence = Some(ctdbdiscconfidence);
        self
    }

    #[allow(dead_code)]
    pub fn comment(&mut self, comment: &str) -> &mut Self {
        self.comment = Some(comment.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn labelno(&mut self, labelno: &str) -> &mut Self {
        self.labelno = Some(labelno.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn disctotal(&mut self, disctotal: u32) -> &mut Self {
        self.disctotal = Some(disctotal);
        self
    }

    #[allow(dead_code)]
    pub fn publisher(&mut self, publisher: &str) -> &mut Self {
        self.publisher = Some(publisher.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn release_date(&mut self, release_date: AlbumDate) -> &mut Self {
        self.release_date = Some(release_date);
        self
    }

    #[allow(dead_code)]
    pub fn genre(&mut self, genre: &str) -> &mut Self {
        self.genre = Some(genre.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn discsubtitle(&mut self, discsubtitle: &str) -> &mut Self {
        self.discsubtitle = Some(discsubtitle.to_owned());
        self
    }

    #[allow(dead_code)]
    pub fn build(&self) -> CueripperMetadata {
        CueripperMetadata {
            fs_path:                self.fs_path.clone(),
            vendor_string:          self.vendor_string.clone(),
            discnumber:             self.discnumber,
            artist:                 self.artist.clone(),
            tracktotal:             self.tracktotal,
            tracknumber:            self.tracknumber,
            title:                  self.title.clone(),
            cdtoc:                  self.cdtoc.clone(),
            album:                  self.album.clone(),
            date:                   self.date.clone(),
            releasecountry:         self.releasecountry.clone(),
            ctdbtrackconfidence:    self.ctdbtrackconfidence,
            ctdbdiscconfidence:     self.ctdbdiscconfidence,
            comment:                self.comment.clone(),
            labelno:                self.labelno.clone(),
            disctotal:              self.disctotal,
            publisher:              self.publisher.clone(),
            release_date:           self.release_date.clone(),
            genre:                  self.genre.clone(),
            discsubtitle:           self.discsubtitle.clone(),
        }
    }
}
