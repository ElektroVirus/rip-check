use crate::music_db::disc_data::DiscData;
use crate::music_db::entry::Entry;
use crate::ripcheck_logger::{RipCheckLogger, RipCheckLoggerMessage};
use anyhow::{anyhow, Result};
use std::fs;
use std::path::{Path, PathBuf};
use std::{env, io};
use subprocess::{Popen, PopenConfig, Redirection};

///
/// External checks API is documented in README
///

#[allow(dead_code)]
struct ExternalCheckOutput {
    cwd: PathBuf,
    exe: PathBuf,
    ret: u32,
    stdout: String,
    stderr: String,
}

impl ExternalCheckOutput {
    pub fn new(cwd: &Path, exe: &Path, ret: u32, stdout: &str, stderr: &str) -> Self {
        ExternalCheckOutput {
            cwd: cwd.to_path_buf(),
            exe: exe.to_path_buf(),
            ret,
            stdout: stdout.to_owned(),
            stderr: stderr.to_owned(),
        }
    }
}

///
/// This function iterates over all external checks in the given
/// directory for external checks, and runs each external check
/// using subprocesses to the given entry.
///
pub fn run_external_checks_for_entry(
    entry: &Entry,
    check_dir: &Path,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    let paths = fs::read_dir(check_dir).unwrap();
    for path in paths {
        if path.as_ref().is_err() {
            rclogger.log(&RipCheckLoggerMessage::Info(
                "external_checks::run_external_checks_for_entry",
                format!("Something wrong with path: {:?}", path),
            ))?;
            continue;
        }
        let test_bin = path.as_ref().unwrap().path();
        run_external_check_for_entry(entry, &test_bin, rclogger)?;
    }
    Ok(())
}

fn run_external_check_for_entry(
    entry: &Entry,
    test_bin: &Path,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    match entry {
        Entry::SingleDiscAlbum(_ad, dd) => {
            check_discdata(dd, test_bin, rclogger)?;
        }
        Entry::ManyDiscAlbum(_ad, ddv) => {
            for dd in ddv {
                check_discdata(dd, test_bin, rclogger)?;
            }
        }
        Entry::ManyDiscAlbumWithRoot(_ad, ddv) => {
            for dd in ddv {
                check_discdata(dd, test_bin, rclogger)?;
            }
        }
        Entry::NoneDiscAlbum(_ad) => {
            eprintln!("external_checks::external_check: skipping NoneDiscAlbum");
        }
    };

    Ok(())
}

fn check_discdata(
    dd: &DiscData,
    test_bin: &Path,
    rclogger: &mut RipCheckLogger,
) -> Result<(), io::Error> {
    match subprocess(&dd.fs_path(), test_bin) {
        Ok(res) => {
            rclogger.log(&RipCheckLoggerMessage::Info(
                "external_checks::check_discdata",
                format!("Subprocess return code: {}", res.ret),
            ))?;
            rclogger.log(&RipCheckLoggerMessage::Info(
                "external_checks::check_discdata",
                format!("Subprocess stdout: {}", res.stdout),
            ))?;
            rclogger.log(&RipCheckLoggerMessage::Info(
                "external_checks::check_discdata",
                format!("Subprocess stderr: {}", res.stderr),
            ))
        }
        Err(e) => rclogger.log(&RipCheckLoggerMessage::Error(
            "external_checks::check_discdata",
            format!("Subprocess failed: {}", e),
        )),
    }
}

///
/// destination: album / disc directory, which will be the new working directory
/// exe: test executable to run. Either absoulute path or relative path from
///     the programs working directory. Not relative from album / disc directory.
///
/// Not, it is better to give at least the path of 'exe' in absoulte form,
/// SingleDiscAlbum the working directory will be changed to 'destination' so a
/// relative path to 'exe' is harder to get.
///
fn subprocess(workdir: &Path, exe: &Path) -> Result<ExternalCheckOutput, anyhow::Error> {
    let exe_absolute = if exe.is_relative() {
        let ripcheck_cwd = env::current_dir()?;
        ripcheck_cwd.join(exe)
    } else {
        exe.to_path_buf()
    };

    if !workdir.exists() {
        return Err(anyhow!(
            "Subprocess: workdir does not exist ({})",
            workdir.display()
        ));
    }

    if !exe.exists() {
        return Err(anyhow!(
            "Subprocess: executable does not exist ({})",
            workdir.display()
        ));
    }

    let mut p = match Popen::create(
        &[exe_absolute.clone()],
        PopenConfig {
            stdout: Redirection::Pipe,
            stderr: Redirection::Pipe,
            cwd: Some(workdir.into()),
            ..Default::default()
        },
    ) {
        Ok(res) => res,
        Err(e) => {
            return Err(anyhow!("Subprocess: popen failed with {e}"));
        }
    };

    let (out, err) = p.communicate(None).unwrap();

    let return_code;

    loop {
        // TODO: This will poll the other process until it is ready and block
        // the main thread.
        if let Some(exit_status) = p.poll() {
            // the process has finished
            match exit_status {
                subprocess::ExitStatus::Exited(code) => {
                    return_code = code;
                    break;
                }
                subprocess::ExitStatus::Signaled(signal) => {
                    println!("Subprocess: sinaled with signal: {}", signal);
                }
                subprocess::ExitStatus::Other(val) => {
                    println!("Subprocess: other with value: {}", val);
                }
                subprocess::ExitStatus::Undetermined => {
                    println!("Subprocess: undetermined");
                }
            }
        }
    }

    let stdout = if let Some(printout) = out {
        printout
    } else {
        String::new()
    };
    let stderr = if let Some(printout) = err {
        printout
    } else {
        String::new()
    };

    Ok(ExternalCheckOutput::new(
        workdir,
        &exe_absolute,
        return_code,
        &stdout,
        &stderr,
    ))
}
