use crate::{music_db::music_db_error::MusicDbError, util};
use std::fs::{File, OpenOptions};
use std::io;
use std::io::Write;
use std::path::PathBuf;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

pub enum RipCheckLoggerMessage<S, T: AsRef<str>> {
    TestSucceeded(S, T),
    TestFailed(S, T),
    Info(S, T),
    Warning(S, T),
    Error(S, T),
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct RipCheckLogger {
    color_tty: bool,
    log_into_tty: bool,
    log_into_file: bool,
    logfile_path: Option<PathBuf>, // Path for logfile
    logfile: Option<File>,
}

impl RipCheckLogger {
    pub fn builder() -> RipCheckLoggerBuilder {
        RipCheckLoggerBuilder::new()
    }

    pub fn log<S: AsRef<str> + std::fmt::Display, T: AsRef<str> + std::fmt::Display>(
        &mut self,
        status: &RipCheckLoggerMessage<S, T>,
    ) -> Result<(), io::Error> {
        // status is the first word with 'Ok', 'Error' etc.
        let status_color_fg;
        let location;
        let message;
        let status_str = match status {
            RipCheckLoggerMessage::TestSucceeded(loc, msg) => {
                status_color_fg = Color::Green;
                message = msg;
                location = loc;
                format!("{:>15}", "Test ok: ")
            }
            RipCheckLoggerMessage::TestFailed(loc, msg) => {
                status_color_fg = Color::Magenta;
                message = msg;
                location = loc;
                format!("{:>15}", "Test failed: ")
            }
            RipCheckLoggerMessage::Info(loc, msg) => {
                status_color_fg = Color::Cyan;
                message = msg;
                location = loc;
                format!("{:>15}", "Info: ")
            }
            RipCheckLoggerMessage::Warning(loc, msg) => {
                status_color_fg = Color::Yellow;
                message = msg;
                location = loc;
                format!("{:>15}", "Warning: ")
            }
            RipCheckLoggerMessage::Error(loc, msg) => {
                status_color_fg = Color::Red;
                message = msg;
                location = loc;
                format!("{:>15}", "Error: ")
            }
        };

        //
        // Write to file
        //

        if self.log_into_file {
            if let Err(_res) = self.log_into_file(&status_str, location, message) {
                eprintln!(
                    "RipCheckLogger::log: Could not log into file {:?}",
                    self.logfile_path
                );
            }
        }

        //
        // Write to TTY
        //

        if self.log_into_tty {
            // Color choice depends on whether output is a TTY
            let choice = if atty::is(atty::Stream::Stdout) && self.color_tty {
                ColorChoice::Always
            } else {
                ColorChoice::Never
            };

            let mut stdout;
            if choice == ColorChoice::Always {
                stdout = StandardStream::stdout(ColorChoice::Always);
                stdout.set_color(
                    ColorSpec::new()
                        .set_fg(Some(status_color_fg))
                        .set_bold(true),
                )?;
            } else {
                stdout = StandardStream::stdout(ColorChoice::Never);
            }
            write!(&mut stdout, "{}", status_str)?;
            stdout.reset()?;

            let mut stdout;
            if choice == ColorChoice::Always {
                stdout = StandardStream::stdout(ColorChoice::Always);
                stdout.set_color(ColorSpec::new().set_fg(Some(Color::Yellow)))?;
            } else {
                stdout = StandardStream::stdout(ColorChoice::Never);
            }
            write!(&mut stdout, "{:50}", location)?;
            stdout.reset()?;

            writeln!(&mut stdout, "{}", message)?;
        }

        Ok(())
    }

    fn log_into_file<S: AsRef<str> + std::fmt::Display, T: AsRef<str> + std::fmt::Display>(
        &mut self,
        status_str: &str,
        _: &T,
        message: &S,
    ) -> Result<(), MusicDbError> {
        if self.logfile.is_none() {
            // The idea is to construct the logfile only when needed, e.g.
            // when there is something to log
            // (this function is called the first time)
            let logfile_path = self
                .logfile_path
                .clone()
                .ok_or(MusicDbError::LogfileNotDefined)?;

            // Create and truncate logfile
            self.logfile = OpenOptions::new()
                .write(true)
                .create(true)
                .truncate(true)
                .open(logfile_path)
                .ok();
        };

        // Actual logging done here
        if let Err(e) = writeln!(
            self.logfile
                .as_mut()
                .ok_or(MusicDbError::LogfileNotDefined)?,
            "{}{}",
            status_str,
            message
        ) {
            eprintln!("Couldn't write to file: {}", e);
        }

        Ok(())
    }
}

pub struct RipCheckLoggerBuilder {
    color_tty: bool,
    log_into_tty: bool,
    log_into_file: bool,
    logfile_name: String,
    album_path: Option<PathBuf>, // Path where to write logfile
}

impl RipCheckLoggerBuilder {
    pub fn new() -> Self {
        RipCheckLoggerBuilder {
            color_tty: false,
            log_into_tty: false,
            log_into_file: false,
            logfile_name: String::from(util::LOGFILE_NAME),
            album_path: None,
        }
    }

    pub fn color_tty(&mut self, color_tty: bool) -> &mut Self {
        self.color_tty = color_tty;
        self
    }

    pub fn log_into_tty(&mut self, log_into_tty: bool) -> &mut Self {
        self.log_into_tty = log_into_tty;
        self
    }

    pub fn log_into_file(&mut self, log_into_file: bool) -> &mut Self {
        self.log_into_file = log_into_file;
        self
    }

    pub fn album_path(&mut self, album_path: PathBuf) -> &mut Self {
        self.album_path = Some(album_path);
        self
    }

    pub fn build(&self) -> RipCheckLogger {
        let logfile_path = self
            .album_path
            .as_ref()
            .map(|ap| ap.join(self.logfile_name.clone()));

        RipCheckLogger {
            color_tty: true,
            log_into_tty: self.log_into_tty,
            log_into_file: self.log_into_file,
            logfile_path,
            logfile: None,
        }
    }
}
