use config::Config;
use config::ConfigError;
use log::{info, warn};
use std::collections::HashMap;

const SETTINGS_FILE_PATH: &str = "cfg/config.toml";

pub struct RipCheckSettings {
    // TODO: Quite unnecessary that this struct contains only one field
    pub config: Config,
}

impl RipCheckSettings {
    pub fn new() -> Self {
        match settings_with_file() {
            Ok(v) => {
                // TODO: Use logging library
                // https://docs.rs/log/latest/log/
                info!("settings_with_file ok");
                return RipCheckSettings { config: v };
            }
            Err(_e) => {
                warn!("settings_with_file failed");
                match settings_without_file() {
                    Ok(v) => {
                        info!("settings_without_file ok");
                        return RipCheckSettings { config: v };
                    }
                    Err(_e) => {
                        warn!("settings_without_file failed");
                    }
                }
            }
        }
        let config = default_settings().expect("failed to read default settings");
        RipCheckSettings { config }
    }

    pub fn debug_print(&self) {
        println!(
            "{:?}",
            &self
                .config
                .clone()
                .try_deserialize::<HashMap<String, HashMap<String, String>>>()
                .unwrap()
        );
    }
}

fn settings_with_file() -> Result<Config, ConfigError> {
    Config::builder()
        // TODO: Default values?
        .add_source(config::File::with_name(SETTINGS_FILE_PATH))
        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
        .add_source(config::Environment::with_prefix("APP"))
        .build()
}

fn settings_without_file() -> Result<Config, ConfigError> {
    Config::builder()
        .add_source(config::Environment::with_prefix("APP"))
        .build()
}

fn default_settings() -> Result<Config, config::ConfigError> {
    Config::builder().set_default("path", "flac")?.build()
}
