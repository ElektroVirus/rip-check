pub const ALBUM_DELIMETER: &str = " - ";
pub const TRACK_DELIMETER: &str = " - ";
pub const METADATA_CONFIDENCE_DELIMETER: &str = "/";
pub const LOGFILE_NAME: &str = "ripcheck.log";

pub fn replace_characters(fname: &str) -> String {
    // TODO: This list is not comprehensive
    fname.to_owned().replace(['/', ':'], "_")
}
